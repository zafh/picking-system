/*******************************
 * OPL 12.8.0.0
 * Author: Richard Verbeet
 * Organisation: Technische Hochschule Ulm
 * Creation Date: 27.05.2020
 *******************************/

using CP;

//*********************** Variables *******************************************
int WT = ...;
float C_H = ...;
int numArticle = ...;
float L_EC_H = ...;
float C_R = ...;
range Articles = 1..numArticle;
float L_EC_R = ...;
int D_F[Articles] = ...;

//*********************** Decision Variables **********************************
dvar int+ SubsetRobot;
dvar int+ SubsetHuman;
dvar int+ scalePOD;
dexpr float POD = scalePOD/100;
dvar int scaleL_EC_H_SR;
dexpr float L_EC_H_SR = scaleL_EC_H_SR/100;
dvar int scaleL_EC_R_SR;
dexpr float L_EC_R_SR = scaleL_EC_R_SR/100;
                                                    
//*********************** Goal Function ***************************************
minimize POD;

//*********************** Constraints *****************************************
subject to {
    sum(i in Articles)(D_F[i]) == SubsetRobot + SubsetHuman;    
    L_EC_H_SR == SubsetRobot * L_EC_H * (100 - POD);
    L_EC_R_SR == SubsetRobot * L_EC_R * (100 - POD);    
    SubsetRobot <= (C_R * WT) - (L_EC_R_SR / WT);
    SubsetHuman <= (C_H * WT) - (L_EC_H_SR / WT);
};