# Picking System #

### Beschreibung ###
Die Software wurde als Demonstrator für ein kooperatives Mensch-Roboter-Kommissioniersystem entworfen, um die Auswirkungen der Verbesserung der Objekterkennung durch eine Feedback-Loop zu untersuchen. Für die Entwicklung für ein Multiagenten-System in JADE/JADEX implementiert.

### Systemanforderungen ###
- Der Demonstrator verwendet Jadex Version 3.0.115. Eine damit kompatible Java-Version ist 1.8.0_261.
- Die Auftragssteuerung des Demonstrators basiert auf einer Optimierung durch CPLEX/OPL. Für den Demonstrator würde die API des OPL-Studios v12.8.0.0 verwendet.
- Als Umgebung zur Objekterkennung wird eine Windows-Installation von Darknet/Yolo (v3) verwendet.
- Für die Darstellung von Bildern in der GUI des Emergency Call wird 'thumbnailator' benutzt.

### Multiagenten-System ###
Die Komponenten im Kommissioniersytem werden durch BDI-Agenten modellier und Aufträge bei Systemstart aus einer Datei geladen oder nach vorgegebenen Parametern generiert werden. Ein Monitor-Agent stellt eine Benutzeroberfläche für die Überwachung der Auftragsbearbeitung bereit. Das Multiagenten-System sowie das Szenario werden in der Dokumentation (Downloads -> Dokumentation_Picking_System.pdf) ausführlich beschrieben.

#### Human Picker Agent ####
Repräsentiert einen menschlichen Kommissionierer. Ist für das Bearbeiten von Kommissionier-Aufträgen zuständig und reagiert auf Support-Anfragen (Emergency Call) von Robotern.

#### Robot Picker Agent ####
Repräsentiert einen Kommissionier-Roboter. Ist für das Bearbeiten von Kommissionier-Aufträgen zuständig und kann bei nicht erfolgreicher Objekterkennung Support-Anfragen (Emergency Call) an menschliche Kommissionierer stellen.

#### Warehouse Agent ####
Repräsentiert ein Warehouse-ManagementSystem, das für die Freigabe von Aufträgen und der Initialisierung der Vergabe (Auktion) zuständig ist.

#### Monitor Agent ####
Wird für das Monitoring des Agenten-Systems verwendet. Alle anderen Agenten registrieren sich nach Start bei diesem Agenten und können ihm Statusmeldungen schicken, die er in einer GUI anzeigt. Es werden keine Nachrichten an die anderen Agenten verschickt, d.h. der Monitor-Agent nimmt keinen Einfluss auf die Simulation.

### Acknowledgment ###
This work is part of the project “ZAFH Intralogistik”, funded by the European Regional Development Fund and the Ministry of Science, Research and Arts of Baden-Württemberg, Germany (F.No. 32-7545.24-17/3/1). It is also done within the post graduate school “Cognitive Computing in Socio-Technical Systems“ of Ulm University of Applied Sciences and Ulm University, which is funded by Ministry for Science, Research and Arts of the State of Baden-Württemberg, Germany.