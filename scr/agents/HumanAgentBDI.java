package agents;

import java.util.ArrayList;
import java.util.List;
import general.WarehouseFactory;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanPrecondition;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import objects.Article;
import objects.PickingOrder;
import objects.Warehouse;
import objects.WayPoint;
import services.IHumanPickerService;
import services.IMonitorService;
import services.IPickerService;
import services.IRobotPickerService;
import services.IWarehouseService;

@Agent
@Arguments({
	@Argument(name="argArticles", clazz=List.class),
	@Argument(name="argWayPoint", clazz=String.class),
	@Argument(name="argMovementSpeed", clazz=Double.class),
	@Argument(name="argTimePickArticle", clazz=Integer.class),
	@Argument(name="argTimeDeliverPickingOrder", clazz=Integer.class),
	@Argument(name="argCapacity", clazz=Integer.class),
	@Argument(name="argSpeedFactor", clazz=Integer.class),
	@Argument(name="argType", clazz=String.class),
	@Argument(name="argMaxSizeOrderList", clazz=Integer.class)
	})
@ProvidedServices({
	@ProvidedService(type=IPickerService.class),
	@ProvidedService(type=IHumanPickerService.class)
})
@RequiredServices({
	@RequiredService(name="WarehouseService", type=IWarehouseService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="RobotPickerService", type=IRobotPickerService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="MonitorService", type=IMonitorService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM))
})
public class HumanAgentBDI implements IPickerService, IHumanPickerService {
		
	//-------- arguments --------
	
	@AgentArgument 
	protected List<Article> argArticles;
	
	@AgentArgument
	protected String argWayPoint;
	
	@AgentArgument
	protected double argMovementSpeed;
	
	@AgentArgument
	protected int argTimePickArticle;
	
	@AgentArgument
	protected int argTimeDeliverPickingOrder;
	
	@AgentArgument
	protected int argCapacity;
	
	@AgentArgument
	protected int argSpeedFactor;
	
	@AgentArgument
	protected String argType;
	
	@AgentArgument
	protected int argMaxSizeOrderList;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess Agent;

	protected double MovementSpeed;	
	protected int TimePickArticle;
	protected int TimeDeliverPickingOrder;
	protected int Capacity;
	
	protected int SpeedFactor;
	protected int MaxSizeOrderList;
	
	protected IMonitorService MonitorService;
	
	//-------- beliefs ---------
	
	@Belief
	protected String AgentName;
		
	@Belief
	protected List<Article> Articles = new ArrayList<Article>();
	
	@Belief
	protected WayPoint CurrentWayPoint;
	
	@Belief
	protected List<PickingOrder> PickingOrders = new ArrayList<PickingOrder>();
	
	@Belief
	protected Warehouse Warehouse;

	@Belief
	protected int FinishedPickingOrders;
	
	@Belief
	protected String Type;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
				
		// set speed factor
		SpeedFactor = argSpeedFactor;
		
		// create warehouse (class Warehouse  couldn't successfully realized as serializable, therefor local instance)
		WarehouseFactory WF = new WarehouseFactory();
		Warehouse = WF.CreateWarehouse();
		
		// load arguments
		MovementSpeed = argMovementSpeed;
		TimePickArticle = argTimePickArticle;
		TimeDeliverPickingOrder = argTimeDeliverPickingOrder;
		Capacity = argCapacity;
		
		MaxSizeOrderList = argMaxSizeOrderList;
		Type = argType;
		
		Articles.addAll(argArticles);		
		CurrentWayPoint = Warehouse.getTransferPlaces().get(argWayPoint);

		// read local name
		AgentName = Agent.getComponentIdentifier().getLocalName();
		
		// get monitoring service and register
		MonitorService = (IMonitorService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("MonitorService").get();
		MonitorService.Register(AgentName);
		
		// adopt global goal
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new FinishPickingOrders());
		
		System.out.println("HumanAgent " + AgentName + " gestartet.");
		
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal(unique=true, deliberation=@Deliberation(inhibits={FinishPickingOrders.class}))
	public class SupportRobot {
		
		@GoalParameter
		protected boolean Status;
		
		protected WayPoint WayPointRobot;	
		protected String Robot;
		
		public SupportRobot(WayPoint wp, String robot) {
			this.WayPointRobot = wp;
			this.Robot = robot;
			this.Status = false;
		}
		
		public WayPoint getWayPointRobot() {
			return this.WayPointRobot;
		}
		
		public String getRobot() {
			return this.Robot;
		}
		
		public void setStatus(Boolean status) {
			this.Status = status;
		}
		
		@GoalTargetCondition(beliefs= {"CurrentWayPoint"})
		public boolean checkTarget() {
			return (WayPointRobot==CurrentWayPoint && Status==true);
		}
		
	}
	
	@Goal(excludemode=ExcludeMode.Never, unique=true)
	public class FinishPickingOrders {
		
		@GoalTargetCondition(beliefs={"PickingOrders"})
		public boolean checkTarget() {
			return PickingOrders.size() == 0;
		}
		
		@GoalMaintainCondition(beliefs={"PickingOrders"})
		public boolean checkMaintain() {
			return PickingOrders.size() == 0;
		}
	
	}
	
	@Goal(retry=true)
	public class MakeProposal {
		
		@GoalParameter
		protected int proposal = -1;
		
		protected PickingOrder order;
	
		public MakeProposal(PickingOrder order) {
			this.order = order;
		}
		
		public PickingOrder getPickingOrder() {
			return this.order;
		}
		
		public void setProposal(int value) {
			this.proposal = value;
		}
		
		@GoalResult
		protected int getResult() {
			return proposal;
		}
			
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return proposal > -1;
		}
			
	}
	
	//-------- plans ----------
	
	@Plan(trigger=@Trigger(goals=SupportRobot.class))
    protected void EmergencyCall(SupportRobot goal, IPlan plan) {
		
		int tMovement;
		
		// remember current waypoint
		WayPoint StartWayPoint = CurrentWayPoint;
		
		// move to robot's waypoint
		tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, goal.getWayPointRobot()) / MovementSpeed);
		plan.waitFor(tMovement*(1000/SpeedFactor)).get();			
		CurrentWayPoint = goal.getWayPointRobot();
		MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " (Start Emergency Call) erreicht.");

		// contact robot and wait for emergency call
		IRobotPickerService[] RobotPickerServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("RobotPickerService").get().toArray(new IRobotPickerService[0]);					
		for(int i=0; i<RobotPickerServices.length; i++) {     
	        IRobotPickerService service = RobotPickerServices[i];             
	        String name = service.RequestName().get();
	        if (name.equals(goal.getRobot())) {
	        	goal.setStatus(service.EmergencyCall().get());
	        	break;
	        }
	    }

		// move to start waypoint
		tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, StartWayPoint) / MovementSpeed);
		plan.waitFor(tMovement*(1000/SpeedFactor)).get();			
		CurrentWayPoint = goal.getWayPointRobot();
		MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " (Finish Emergency Call) erreicht.");
		
	}
	
	@Plan(trigger=@Trigger(goals=MakeProposal.class))
    protected void EvaluatePickingOrder(MakeProposal goal) {
		
		PickingOrder order = goal.getPickingOrder();		
		WayPoint WP = CurrentWayPoint;
		
		// Berechnung von tFinish
		int tFinish = 0;
		for (PickingOrder pickingorder : PickingOrders) {			
			for (Tuple2<Article,Integer> position : pickingorder.getPositions()) {					
				// Berechnung der Wegdauer analog StartPicking
				WayPoint newWP = Warehouse.getShelfs().get(position.getFirstEntity().getShelf());
				tFinish  = tFinish  + (int) (Warehouse.getDistance(WP, newWP) / MovementSpeed);
				WP = newWP;
				// Berechung der Pick-Dauer analog StartPicking <-- Hier kann es zu einer Ungenauigkeit kommen, wenn der Auftrag schon begonnen wurde.
				tFinish  = tFinish + position.getSecondEntity() * TimePickArticle;
			}
			// Berechnung der abschließenden Ablieferung analog StartPicking
			WayPoint TP = Warehouse.getTransferPlaces().get(order.getTransferPlace());
			tFinish  = tFinish + (int) (Warehouse.getDistance(WP, TP) / MovementSpeed);
			WP = TP;
			tFinish  = tFinish + TimeDeliverPickingOrder;
		}
			
		// Berechnung von tMovement
		int tMovement = 0;
		// Berechnung der Wegdauer zur ersten Position in "order" analog StartPicking
		WayPoint newWP = Warehouse.getShelfs().get(order.getPositions().get(0).getFirstEntity().getShelf());
		tMovement  = tMovement  + (int) (Warehouse.getDistance(WP, newWP) / MovementSpeed);
		
		// Berechnung von tEmergencyCall
		int tEmergencyCall = 0;
		
		goal.setProposal(tFinish + tMovement + tEmergencyCall);
			
	}
	
	@Plan(trigger=@Trigger(goals=FinishPickingOrders.class))
	public class Picking {
	
		@PlanAPI
		RPlan plan;
		
		@PlanPrecondition
		protected Boolean PlanPrecondition() {
		    return PickingOrders.size() != 0;
		}
		
		@PlanBody
		public void Body() {
			
			PickingOrder order = PickingOrders.get(0);
			
			MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " gestartet.");
			
			for (Tuple2<Article,Integer> position : order.getPositions()) {
				
				// move to shelf
				WayPoint DestinationWayPoint = Warehouse.getShelfs().get(
						position.getFirstEntity().getShelf()
				);
				int tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, DestinationWayPoint) / MovementSpeed);
				plan.waitFor(tMovement*(1000/SpeedFactor)).get();			
				CurrentWayPoint = DestinationWayPoint;
				MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " erreicht.");
				
				// pick article(s)
				int tPicking = position.getSecondEntity() * TimePickArticle;
				plan.waitFor(tPicking*(1000/SpeedFactor)).get();
				MonitorService.Status(AgentName, AgentName + ": Position (" + position.getSecondEntity() + "x " + position.getFirstEntity().getName() + ") wurde kommissioniert.");
	
			}

			// finish order					
			IWarehouseService service = (IWarehouseService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("WarehouseService").get();
			service.FinishPickingOrder(order.getOrderID());
			MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " abgeschlossen.");
			
			/**
			// move to transfer place after finishing 2 picking orders
			FinishedPickingOrders++;
			if (FinishedPickingOrders == Capacity) {
			
				WayPoint DestinationWayPoint = Warehouse.getTransferPlaces().get(
						order.getTransferPlace()
				);
				int tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, DestinationWayPoint) / MovementSpeed);
				plan.waitFor(tMovement*(1000/SpeedFactor)).get();
				CurrentWayPoint = DestinationWayPoint;
				MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " erreicht.");
				
				// deliver picking order
				plan.waitFor(TimeDeliverPickingOrder*(1000/SpeedFactor)).get();
				MonitorService.Status(AgentName, AgentName + ": Aufträge wurden an Übergabepunkt abgeliefert.");
				FinishedPickingOrders = 0;
				
			}
			**/
			
			// remove order -> triggers next order
			PickingOrders.remove(order);
			
		}
		
	}

	//-------- methods --------
	
	//-------- services --------
	
	public IFuture<Boolean> AssignPickingOrder(PickingOrder order) {

		final Future<Boolean> value = new Future<Boolean>();
		
		PickingOrders.add(order);
		//MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " angenommen.");
		
		value.setResult(true);		
		return value;

	}

	public IFuture<Integer> CallForProposal(PickingOrder order) {

		final Future<Integer> result = new Future<Integer>();

		int value = Integer.MAX_VALUE;
		
		if (Agent.getComponentFeature(IBDIAgentFeature.class).getGoals(SupportRobot.class).isEmpty() && PickingOrders.size() <= MaxSizeOrderList)
			value = (int) Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new MakeProposal(order)).get();

		result.setResult(value);		
		return result;
		
	}
	
	public IFuture<Integer> RequestSupport(String wp) {
		
		final Future<Integer> result = new Future<Integer>();
		
		int tMovement = Integer.MAX_VALUE;
		
		if (Agent.getComponentFeature(IBDIAgentFeature.class).getGoals(SupportRobot.class).isEmpty())
			tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, Warehouse.getWayPoint(wp)) / MovementSpeed);
		
		result.setResult(tMovement);		
		return result;
				
	}
	
	public IFuture<Boolean> AssignSupportOrder(String wp, String robot) {
		
		final Future<Boolean> value = new Future<Boolean>();
		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new SupportRobot(Warehouse.getWayPoint(wp), robot));
		
		value.setResult(true);		
		return value;
		
	}
	
	public IFuture<String> RequestType() {
		
		final Future<String> value = new Future<String>();

		value.setResult(Type);		
		return value;
		
	}
	
}