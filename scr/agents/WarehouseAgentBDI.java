package agents;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import general.PickingOrderFactory;
import general.WarehouseFactory;
import ilog.concert.IloException;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bdiv3.runtime.IPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IExecutionFeature;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.CollectionResultListener;
import jadex.commons.future.DelegationResultListener;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.commons.future.IResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import objects.Article;
import objects.PickingOrder;
import objects.Warehouse;
import services.IMonitorService;
import services.IPickerService;
import services.IWarehouseService;

@Agent
@Arguments({
	@Argument(name="argArticles", clazz=List.class),
	@Argument(name="argMaxPosition", clazz=Integer.class),
	@Argument(name="argMaxQuantity", clazz=Integer.class),
	@Argument(name="argInterarrivalTime", clazz=Double.class),
	@Argument(name="argSpeedFactor", clazz=Integer.class),
	@Argument(name="argWorkingTime", clazz=Integer.class),
	@Argument(name="argPathOrderList", clazz=String.class),
	@Argument(name="argPreselectionOrderAssignment", clazz=Boolean.class),
	@Argument(name="argIntervallRollingHorizons", clazz=Integer.class),
	@Argument(name="argL_EC_H", clazz=Double.class),
	@Argument(name="argL_EC_R", clazz=Double.class),
	@Argument(name="argC_H", clazz=Double.class),
	@Argument(name="argC_R", clazz=Double.class)
})
@ProvidedServices({
	@ProvidedService(type=IWarehouseService.class)
})
@RequiredServices({
	@RequiredService(name="PickerService", type=IPickerService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="MonitorService", type=IMonitorService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM))
})
public class WarehouseAgentBDI implements IWarehouseService {
	
	//-------- arguments --------
	
	@AgentArgument 
	protected List<Article> argArticles;
	
	@AgentArgument
	protected int argMaxPosition;
	
	@AgentArgument
	protected int argMaxQuantity;
	
	@AgentArgument
	protected double argInterarrivalTime;
	
	@AgentArgument
	protected int argSpeedFactor;
	
	@AgentArgument
	protected int argWorkingTime;
	
	@AgentArgument
	protected String argPathOrderList;
	
	@AgentArgument
	protected Boolean argPreselectionOrderAssignment;
	
	@AgentArgument
	protected int argIntervalRollingHorizons;
	
	@AgentArgument
	protected double argL_EC_H;
	
	@AgentArgument
	protected double argL_EC_R;
	
	@AgentArgument
	protected double argC_H;
	
	@AgentArgument
	protected double argC_R;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess Agent;
	
	protected String AgentName;	
	protected PickingOrderFactory POF;
	
	protected IMonitorService MonitorService;	
	
	protected double InterarrivalTime;	
	protected int SpeedFactor;
	
	protected int WorkingTime;
	protected double L_EC_H;
	protected double L_EC_R;
	protected double C_H;
	protected double C_R;
	protected int[] D_F;
	
	protected String PathOrderList;
	
	protected Boolean PreselectionOrderAssignment;
	protected int IntervalRollingHorizons;
	
	protected BufferedWriter BW;
	protected int CounterOpen;
	protected int CounterAssigned;
	protected int CounterComplete;
	
	protected long TimeStart;
	protected long TimeCurrent;	

	//-------- beliefs --------
	
	@Belief
	protected List<Article> Articles = new ArrayList<Article>();
	
	@Belief
	protected List<PickingOrder> PickingOrders = new ArrayList<PickingOrder>();
	
	@Belief
	protected Warehouse Warehouse;
	
	@Belief
	protected double ThresholdOrderAssignment = 0.0;

	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() throws IOException {
		
		// set speed factor
		SpeedFactor = argSpeedFactor;
		
		// set working time
		WorkingTime = argWorkingTime;
		
		// set saving path for order list
		PathOrderList = argPathOrderList;

		PreselectionOrderAssignment = argPreselectionOrderAssignment;
		IntervalRollingHorizons = argIntervalRollingHorizons;
		TimeStart = (long) (System.currentTimeMillis() / 1000);
		L_EC_H = argL_EC_H;
		L_EC_R = argL_EC_R;
		C_H = argC_H;
		C_R = argC_R;
		
		Articles.addAll(argArticles);

		WarehouseFactory WF = new WarehouseFactory();
		Warehouse = WF.CreateWarehouse();
		
		POF = new PickingOrderFactory();
		POF.setArticles(Articles);
		POF.setTransferPlace("TransferStation");
		POF.setMaxPosition(argMaxPosition);
		POF.setMaxQuantity(argMaxQuantity);		
		POF.importPickingOrders("C:\\Photoneo\\Simulation\\PickingOrders.txt");
		D_F = POF.getDemand();
		
		InterarrivalTime = argInterarrivalTime;
		
		// read local name
		AgentName = Agent.getComponentIdentifier().getLocalName();
		
		// set monitoring service and register
		MonitorService = (IMonitorService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("MonitorService").get();
		MonitorService.Register(AgentName);
		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new PublishOrderStatus());
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new RollingHorizons());
		
		System.out.println("WarehouseAgent gestartet.");
		
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() throws IOException {
		
		/* Generierung Auftr�ge
		 * 	 (a) vorgegeben aus Text-Datei
		 *   (b) zufallsgenerierte Auftr�ge (auskommentiert)
		 * Verfahren Auftragsfreigabe
		 *   (a) Poisson-verteilt durch Vorgabe einer Zwischenankunftszeit
		 *   (b) gleichverteilt basierend auf Working Time (auskommentiert - setzt Auftragsliste aus Text-Datei voraus)
		 */
		
		Random r = new Random();
		double Lambda = 1 / InterarrivalTime;
		// int interval = (int) (WorkingTime / POF.getListPickingOrder().size());
		
		// write released orders to file
		File file = new File (PathOrderList  + "\\OrderList.txt");
		file.createNewFile();
		BW = new BufferedWriter(new FileWriter(file));
		
		while (true) {
			
			// create picking order and attach it to goal
			// PickingOrder order = POF.generatePickingOrder();
			PickingOrder order = POF.getPickingOrder();
			PickingOrders.add(order);
			AssignPickingOrder NewGoal = new AssignPickingOrder(order);			
			Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(NewGoal);
			CounterOpen++;

			BW.write(order.getOrderID() + "; " + order.getTransferPlace() + "; ");
			for (Tuple2<Article,Integer> position : order.getPositions())
				BW.write(position.getFirstEntity().getName() + ";1;");
			BW.write("\n");
			BW.flush();
			
			if (POF.getListPickingOrder().indexOf(order) == POF.getListPickingOrder().size()-1 )
				break;
			
			// start a new order according to interarrival time;		
			int t = (int) (-Math.log(1.0 - r.nextDouble()) / Lambda);	
			Agent.getComponentFeature(IExecutionFeature.class).waitForDelay(t*(1000/SpeedFactor)).get();		
			
			// Agent.getComponentFeature(IExecutionFeature.class).waitForDelay(interval*(1000/SpeedFactor)).get();

		}
		
		MonitorService.Status(AgentName, AgentName + ": Alle Auftr�ge der Liste wurden ausgelesen!");
		
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() throws IOException {
		
		BW.close();
		
	}
		
	//-------- goals --------
	
	@Goal(unique=true, recur=true) 
	public class PublishOrderStatus {
		
		@GoalParameter
		protected boolean maintain;
		
		public PublishOrderStatus() {
			this.maintain = false;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return maintain;
		}
			
	}
	
	@Goal(unique=true, recur=true) 
	public class RollingHorizons {
		
		@GoalParameter
		protected boolean maintain;
		
		public RollingHorizons() {
			this.maintain = false;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return maintain;
		}
			
	}
	
	@Goal(retry=true, recur=true, recurdelay=5000) 
	public class AssignPickingOrder {
		
		@GoalParameter
		protected PickingOrder order;
	
		public AssignPickingOrder(PickingOrder order) {
			this.order = order;
		}
		
		public PickingOrder getPickingOrder() {
			return this.order;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return order.getStatus().equals(PickingOrder.ASSIGNED);
		}
			
	}
		
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=PublishOrderStatus.class))
	protected void Publish(IPlan plan) throws IOException {
				
		while (true) {
		
			MonitorService.Status(AgentName, AgentName + ": Status Auftr�ge > OPEN=" + String.valueOf(CounterOpen) + " | ASSIGNED=" + String.valueOf(CounterAssigned) + " | COMPLETE=" + String.valueOf(CounterComplete));
			
			plan.waitFor(5000).get();
		
		}
		
	}
	
	@Plan(trigger=@Trigger(goals=RollingHorizons.class))
	protected void CalculatePOD(IPlan plan) throws IloException {
				
		while (true) {

	        ThresholdOrderAssignment = 100.0;
	        
	        // Anpassung der Working Time
	    	TimeCurrent = (long) System.currentTimeMillis() / 1000;
	    	int NewWorkingTime = WorkingTime - (int) (TimeCurrent - TimeStart);
	        
	    	// Die Systemparameter bleiben Anzahl Menschen und Roboter, Picking Rate und resultierende Kapazit�ten bleiben unver�ndert und werden durch die Optimierung auf die Working Time bezogen!
	    	
	    	// Anpassung Demand Forecast
	    	int[] NewDemandForecast = D_F.clone();
			for (PickingOrder pickingorder : PickingOrders) {
				if (pickingorder.getStatus().equals(PickingOrder.COMPLETE)) {
					NewDemandForecast[Articles.indexOf(pickingorder.getPositions().get(0).getFirstEntity())]--;
				}
			}

	        ThresholdOrderAssignment = general.PreselectionOrderAssignment.CalculateThreshold(NewWorkingTime, L_EC_H, L_EC_R, C_H, C_R, Articles.size(), NewDemandForecast);	        
			
			MonitorService.Status(AgentName, AgentName + ": Schwellwert f�r Objekterkennung der Order Assignment aktualisiert (" + String.valueOf(ThresholdOrderAssignment) + ")");
					
			plan.waitFor(IntervalRollingHorizons*1000).get();
		
		}
		
	}
	
	@Plan(trigger=@Trigger(goals=AssignPickingOrder.class))
    protected void StartAuction(AssignPickingOrder goal) {
		
		PickingOrder order = goal.getPickingOrder();
		
		MonitorService.Status(AgentName, AgentName + ": Auktion f�r Verteilung von Auftrag " + order.getOrderID() + " gestartet.");
		
        // Contract Net Protocol       
		IPickerService[] PickerServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("PickerService").get().toArray(new IPickerService[0]);            
        
        // create listener object for call for proposal
        Future<Collection<Tuple2<IPickerService, Integer>>> cfp =
        	new Future<Collection<Tuple2<IPickerService, Integer>>>();        
        final CollectionResultListener<Tuple2<IPickerService, Integer>> crl =
            new CollectionResultListener<Tuple2<IPickerService, Integer>>(
            	PickerServices.length, false, new DelegationResultListener<Collection<Tuple2<IPickerService, Integer>>>(cfp)
            );

        for(int i=0; i<PickerServices.length; i++) {    
            
            IPickerService PickerService = PickerServices[i];
            
            // Ist die Preselektion aktiviert und der POD des Auftrags unterhalb des kritischen POD (ThresholdOrderAssignment), wird der Auftrag nicht an Roboter (Type="Robot") vergeben.
            if (PreselectionOrderAssignment == true && order.getPositions().get(0).getFirstEntity().getPOD() <= ThresholdOrderAssignment && PickerService.RequestType().get().equals("Robot")) {
            	crl.resultAvailable(new Tuple2<IPickerService, Integer>(PickerService, Integer.MAX_VALUE));
            	continue;
            }
            	
            PickerService.CallForProposal(order).addResultListener(new IResultListener<Integer>() {
                
                public void resultAvailable(Integer proposal) {
                    crl.resultAvailable(new Tuple2<IPickerService, Integer>(PickerService, proposal));    
                }    
                
                public void exceptionOccurred(Exception exception) {
                	crl.resultAvailable(new Tuple2<IPickerService, Integer>(PickerService, Integer.MAX_VALUE));
                }
                
            });
                     
        }

        @SuppressWarnings("unchecked")
		Tuple2<IPickerService, Integer>[] proposals = cfp.get().toArray(new Tuple2[0]);
        
        // evaluate proposals
        int value = Integer.MAX_VALUE;
        IPickerService service = null;
        for (Tuple2<IPickerService, Integer> proposal : proposals) {
        	MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " wurde von " + proposal.getFirstEntity().toString().split("@")[1] + " bewertet: " + proposal.getSecondEntity());
        	if (proposal.getSecondEntity() < value) {
        		service = proposal.getFirstEntity();
        		value = proposal.getSecondEntity();
        	}
        }
        
        // accept proposal
        if (service != null) {      
	        service.AssignPickingOrder(order);
	        MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " wurde " + service.toString().split("@")[1] + " zugewiesen.");
	        order.setStatus(PickingOrder.ASSIGNED);
	        CounterOpen--;
	        CounterAssigned++;
        }
        
    }
	
	//-------- methods --------
	
	//-------- services --------

	public IFuture<Boolean> FinishPickingOrder(String id) {

		final Future<Boolean> value = new Future<Boolean>();
		
		for (PickingOrder pickingorder : PickingOrders) {
			if (pickingorder.getOrderID().equals(id)) {
				pickingorder.setStatus(PickingOrder.COMPLETE);
				CounterAssigned--;
				CounterComplete++;
				MonitorService.Status(AgentName, AgentName + ": Auftrag " + id + " wurde abgeschlossen.");
				break;
			}
		}
		
		value.setResult(true);		
		return value;
		
	}
	
	public IFuture<Boolean> UpdatePOD(int id, double pod) {
		
		final Future<Boolean> value = new Future<Boolean>();
		
		for (Article article : Articles) {			
			if (article.getID() == id) {
				article.setPOD(pod);
				break;
			}			
		}
		
		value.setResult(true);		
		return value;
		
	}
	
	public IFuture<HashMap<Article,Integer>> GetForecast() {
		
		final Future<HashMap<Article,Integer>> value = new Future<HashMap<Article,Integer>>();
		
		HashMap<Article,Integer> forecast = new HashMap<Article,Integer>();

		for (int i=0; i<Articles.size(); i++)			
			forecast.put(Articles.get(i),D_F[i]);
				
		value.setResult(forecast);		
		return value;
		
	}
					
}