package agents;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import general.CommandShell;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import objects.Article;
import objects.LearningSet;
import objects.Picture;
import services.IMySQL;
import services.IRobotPickerService;
import services.IWarehouseService;

@Agent
@Arguments({
	@Argument(name="argPathDarknet", clazz=List.class)
})
@RequiredServices({
	@RequiredService(name="MySQL", type=IMySQL.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="WarehouseService", type=IWarehouseService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="RobotPickerService", type=IRobotPickerService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
})
public class ComputationClusterBDI {
		
	//-------- arguments --------
	
	@AgentArgument 
	protected String argPathDarknet;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess Agent;
	
	IMySQL ServiceMySQL;
	
	String PathDarknet;
	String PathCNN;
	
	//-------- beliefs ---------
	
	@Belief
	protected List<Article> Articles;
	
	@Belief
	protected HashMap<Article,Integer> Forecast;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		PathDarknet = argPathDarknet;
				
		// get service "MySQL"
		System.out.println("");
		System.out.println("Start search for provider of service MySQL...");
		IMySQL[] MySQL = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("MySQL").get().toArray(new IMySQL[0]);					
		for(int i=0; i<MySQL.length; i++) {			
			ServiceMySQL = MySQL[i];
			System.out.println("Established connection to provider of service MySQL.");
		}
		
		// import articles from database
		Articles = importArticles();
		
		// request forecast from warehouse agent
		IWarehouseService[] WarehouseServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("WarehouseService").get().toArray(new IWarehouseService[0]);	
	    for(int i=0; i<WarehouseServices.length; i++) { 	
	        IWarehouseService WarehouseService = WarehouseServices[i];
	        Forecast = WarehouseService.GetForecast().get();
	    }

		// create goal to train model for object detection
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new ImproveObjectDetection());	
		
		System.out.println("Computation Cluster " + Agent.getComponentIdentifier().getLocalName() + " gestartet.");
		
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal(excludemode=ExcludeMode.Never, unique=true) 
	public class ImproveObjectDetection {
		
		@GoalMaintainCondition
		public boolean checkMaintain() {			
			boolean result = false;		
			for (Article article : Articles)			
				if (article.getPOD() < 100.0)
					result = true;					
			return result;
		}
	
	}
	
	//-------- plans ----------
		
	@Plan(trigger=@Trigger(goals=ImproveObjectDetection.class))
	public class StartTraining {
	
		@PlanAPI
		RPlan plan;

		@PlanBody
		public void Body() throws InterruptedException, IOException {

			// Berechnung der Rating-Werte und Sortierung der Artikel-Liste
			sortArticles();
			for (Article article : Articles) {

				// Abfrage bei Datenbank, welche Bilder mit Status "Training" f�r den Artikel vorliegen -> Alle Bilder f�r den Artikel mit Status "Training"
				List<Picture> Pictures = importPictures(article);
								
				// Pr�fung, ob Bilder f�r ein Re-Training vorhanden sind
/** ToDo **/	if (Pictures.size() >= 4) { // Aktuell gilt f�r das Training: 75% Training, 25% Test. 4 Bilder gew�hrleisten, dass es immerhin ein Testbild gibt.
				
					// Generierung der Trainingsdaten und Config-Files
					LearningSet LearningSet = new LearningSet();

					List <String> Names = new ArrayList<String>();	
					Names.add(article.getName());
					LearningSet.setClasses(Names);

					List <String> Data = new ArrayList<String>();
					for (Picture picture : Pictures)				
						Data.add(picture.getDatapath());
					LearningSet.setData(Data);

					LearningSet.CreateSets(0.25);
/** ToDo **/		LearningSet.CreateTrainingData(PathDarknet); // Passt das so?
					
					// Start Darknet-Training					
/** ToDo **/		CommandShell cs = new CommandShell(""); // Wie lautet der finish-String? Was ist die Abbruch-Bedingung f�r das Training? Iterationen? Fehler?
	
					String cmdChangeDirectory = "cd " + PathDarknet;
					cs.processInput(cmdChangeDirectory);
					
					PathCNN = article.getCNN();
/** ToDo **/		String cmdTraining = "darknet.exe detector train data/obj.data data/yolo-obj.cfg " + article.getCNN() + " -show_imgsv +"; // Sind die Dateipfade so ok oder muss das dynamisch realisiert werden?
					cs.processInput(cmdTraining);
	
					cs.process.waitFor();
					
					// Auswertung des Trainings					
					double newPOD = 0.0;
					String newCNN = "";
					for (int i=0; i<cs.Output.size(); i++) {
/** ToDo **/			newPOD = 0.0; // Ergibt sich der neue POD direkt aus dem Re-Training oder muss er durch ein separates Verfahren ermittelt werden?					
/** ToDo **/			newCNN = ""; // Namen von CNN f�r Speicherort auslesen bzw. generieren
					}
				
					cs.dispose();
					
					// Werte f�r POD und CNN im Objekt 'article' und der Datanbank aktualisieren		
					article.setPOD(newPOD);
					article.setCNN(newCNN);
					String insert = "UPDATE pickingstation.article "
								  + "SET (`POD`=" + newPOD + ",`CNN`=" + newCNN + ") " 
								  + "WHERE article.`Article-ID`=" + article.getID();
					ServiceMySQL.executeUpdate(insert).get();
					
					// Warehouse und RobotAgent �ber den neuen POD des Artikel und den Speicherort des CNN informieren -> Es m�sste reichen das jeweils im lokalen Objekt "Article" anzupassen			
					IWarehouseService[] WarehouseServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("WarehouseService").get().toArray(new IWarehouseService[0]);	
				    for(int i=0; i<WarehouseServices.length; i++) { 	
				        IWarehouseService WarehouseService = WarehouseServices[i];
				        WarehouseService.UpdatePOD(article.getID(), newPOD);
				    }
				    IRobotPickerService[] RobotPickerServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("RobotPickerService").get().toArray(new IRobotPickerService[0]);	
				    for(int i=0; i<RobotPickerServices.length; i++) { 	
				        IRobotPickerService RobotPickerService = RobotPickerServices[i];
				        RobotPickerService.UpdatePOD(article.getID(), newPOD);
				        RobotPickerService.UpdateCNN(newCNN);
				    }
				    
				    // Status der Bilder in Datenbank aktualisieren
				    String update = "";
				    for (Picture picture : Pictures) {
				    	update = "UPDATE pickingstation.picture "
							   + "SET (`Status`=" + Picture.StatusDetection + ") " 
							   + "WHERE picture.`Picture-ID`=" + picture.getPictureID();
				    	ServiceMySQL.executeUpdate(update).get();
				    }
	
					break;
				
				}

			}

		}
		
	}
			
	//-------- methods --------
	
	public void sortArticles() {
		
		Collections.sort(Articles, new Comparator<Article>() {
			
			@Override
			public int compare(Article article1, Article article2) {
				
				// Rating_Article = Demand_Article,Forecast * P_EOD,Article
				
				double rating1 = Forecast.get(article1) * (1 - article1.getPOD());
				double rating2 = Forecast.get(article2) * (1 - article2.getPOD());
				
				return Double.compare(rating1, rating2);			
			}
			
	    });
		
	}
	
	public List<Article> importArticles() {
		
		List<Article> articles = new ArrayList<Article>();
		List<HashMap<String,Object>> result;
		
		int ID;
		double POD = 0.0;
		String Name = "";
		String Group = "";
		String CNN = "";
		
		String select = "SELECT * FROM pickingstation.article LEFT OUTER JOIN pickingstation.model ON article.`ModelC` = model.`Model-ID`";
		result = ServiceMySQL.executeSelect(select).get();

		for (HashMap<String,Object> article : result) {
			
			ID = (int) article.get("Article-ID");
			
			if (article.get("Name") != null) 
				Name = (String) article.get("Name");
			
			if (article.get("Group") != null)
				Group = (String) article.get("Group");
			
			if (article.get("POD") != null)
				POD = (double) article.get("POD");
			
			if (article.get("Datapath") != null)
				CNN = (String) article.get("Datapath");
			
			articles.add(new Article(ID, Name, Group, "", POD, CNN, ""));
		}
		
		return articles;
		
	}
	
	public List<Picture> importPictures(Article article) {
		
		List<Picture> pictures = new ArrayList<Picture>();
		List<HashMap<String,Object>> result;
		
		String PictureID = "";
		int Index = 0;
		String Status = "";
		String Datapath = "";
		
		String select = "SELECT * FROM pickingstation.picture WHERE picture.`Status`='Training'";
		result = ServiceMySQL.executeSelect(select).get();

		for (HashMap<String,Object> picture : result) {
			
			PictureID = (String) picture.get("Picture-ID");
			
			if (picture.get("Index") != null) 
				Index = (int) picture.get("Index");

			if (picture.get("Status") != null) 
				Status = (String) picture.get("Status");
			
			if (picture.get("Datapath") != null) 
				Datapath = (String) picture.get("Datapath");
					
			pictures.add(new Picture(PictureID, article.getID(), Index, Status, "", 0, 0, "", Datapath, ""));
		}
		
		return pictures;
				
	}
	
	//-------- services --------
	
}