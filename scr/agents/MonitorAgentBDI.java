package agents;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bridge.IInternalAccess;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import services.IMonitorService;
import window.MonitorWindow;

@Agent
@Arguments({
	@Argument(name="argPathSaveRecord", clazz=String.class)
	})
@ProvidedServices({
	@ProvidedService(type=IMonitorService.class)
})
public class MonitorAgentBDI implements IMonitorService {
	
	//-------- arguments --------
	
	@AgentArgument 
	protected String argPathSaveRecord;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess Agent;

	MonitorWindow window;
	
	String PathSaveRecord;
	
	//-------- beliefs --------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		PathSaveRecord = argPathSaveRecord;
		
		// create GUI
		EventQueue.invokeLater(new Runnable() {	
			public void run() {
				// create window object
				window = new MonitorWindow();
				ConfigureWindow();
				window.frmMonitor.setVisible(true);
			}
		});
		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new SaveRecord(true));
		
		System.out.println("MonitorAgent gestartet.");
		
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal(unique=true, recur=true, recurdelay=30000) 
	public class SaveRecord {
		
		@GoalParameter
		protected boolean maintain;
		
		public SaveRecord(boolean maintain) {
			this.maintain = maintain;
		}
		
		public void setMaintain(boolean maintain) {
			this.maintain = maintain;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return !maintain;
		}
			
	}
	
	@Goal(retry=true, recur=true) 
	public class Register {
		
		protected String name;
	
		public Register(String name) {
			this.name = name;
		}

		protected String getName() {
			return this.name;
		}
			
	}
	
	@Goal(retry=true, recur=true) 
	public class Status {
		
		protected String name;
		protected String status;
	
		public Status(String name, String status) {
			this.name = name;
			this.status = status;
		}

		protected String getName() {
			return this.name;
		}
		
		protected String getStatus() {
			return this.status;
		}
			
	}	
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=SaveRecord.class))
	protected void WriteFile() throws IOException {

		for (int i=0; i<window.TabMenu.getTabCount(); i++ ) {					
			JScrollPane Tab = (JScrollPane) window.TabMenu.getComponentAt(i);
			JTextArea textArea = (JTextArea) Tab.getViewport().getComponents()[0];
			textArea.getText();
			File file = new File (PathSaveRecord  + "\\" + Tab.getName() + ".txt");
			FileWriter fw = new FileWriter(file, false);
			textArea.write(fw);
			fw.close();
		}

	}
	
	@Plan(trigger=@Trigger(goals=Status.class))
	protected void SetStatus(Status goal) {
		
		String name = goal.getName();
		String status = goal.getStatus();
		
		for (int i=0; i<window.TabMenu.getTabCount(); i++ ) {					
			JScrollPane Tab = (JScrollPane) window.TabMenu.getComponentAt(i);
			if (name.equals(Tab.getName())) {
				JTextArea textArea = (JTextArea) Tab.getViewport().getComponents()[0];
				textArea.append(Time() + ": " + status + "\n\r");
			}					
		}
	    
	}
		
	@Plan(trigger=@Trigger(goals=Register.class))
    protected void RegisterAgent(Register goal) {
		
		String name = goal.getName();
		
		JScrollPane Tab = new JScrollPane();
		Tab.setName(name);
		JTextArea textArea = new JTextArea();
		Tab.setViewportView(textArea);
		window.TabMenu.addTab(name, null, Tab, null);

		window.taStatus.append("Agent " + name + " wurde registriert." + "\n\r");
		
	}
	
	//-------- methods --------
	
	public void ConfigureWindow() {
		
	}
	
	public String Time() {		
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");		
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));		
		return formatter.format(new Date());
	}
	
	//-------- services --------

	public void Register(String name) {		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new Register(name));	
	}

	public void Status(String name, String status) {		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new Status(name, status));
	}
					
}