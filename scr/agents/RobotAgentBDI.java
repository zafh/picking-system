package agents;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import general.CommandShell;
import general.WarehouseFactory;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalRecurCondition;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanPrecondition;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.annotation.Timeout;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.CollectionResultListener;
import jadex.commons.future.DelegationResultListener;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.commons.future.IResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import net.coobird.thumbnailator.Thumbnails;
import objects.Article;
import objects.PickingOrder;
import objects.Warehouse;
import objects.WayPoint;
import services.IHumanPickerService;
import services.IMonitorService;
import services.IPickerService;
import services.IRobotPickerService;
import services.IWarehouseService;
import window.EmergencyCallWindow;

@Agent
@Arguments({
	@Argument(name="argArticles", clazz=List.class),
	@Argument(name="argWayPoint", clazz=String.class),
	@Argument(name="argMovementSpeed", clazz=Double.class),
	@Argument(name="argTimePickArticle", clazz=Integer.class),
	@Argument(name="argTimeDeliverPickingOrder", clazz=Integer.class),
	@Argument(name="argCapacity", clazz=Integer.class),
	@Argument(name="argDelayEmergencyCall", clazz=Integer.class),	
	@Argument(name="argPathDarknet", clazz=String.class),
	@Argument(name="argPathData", clazz=String.class),
	@Argument(name="argPathConfig", clazz=String.class),
	@Argument(name="argPathWeights", clazz=String.class),
	@Argument(name="argThresholdObjectDetection", clazz=Double.class),
	@Argument(name="argPathYoloMark", clazz=String.class),
	@Argument(name="argSpeedFactor", clazz=Integer.class),	
	@Argument(name="argTimeEvaluateSituation", clazz=Integer.class),
	@Argument(name="argTimeModifyEnvironment", clazz=Integer.class),
	@Argument(name="argTimeConfirmNewRecord", clazz=Integer.class),
	@Argument(name="argTimeConfirmRelease", clazz=Integer.class),
	@Argument(name="argTimeMarkArticle", clazz=Integer.class),
	@Argument(name="argType", clazz=String.class),
	@Argument(name="argMaxSizeOrderList", clazz=Integer.class)
	})
@ProvidedServices({
	@ProvidedService(type=IPickerService.class),
	@ProvidedService(type=IRobotPickerService.class)
})
@RequiredServices({
	@RequiredService(name="WarehouseService", type=IWarehouseService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="HumanPickerService", type=IHumanPickerService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="MonitorService", type=IMonitorService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM))
})
public class RobotAgentBDI implements IPickerService, IRobotPickerService {
		
	//-------- arguments --------
	
	@AgentArgument 
	protected List<Article> argArticles;
	
	@AgentArgument
	protected String argWayPoint;
	
	@AgentArgument
	protected double argMovementSpeed;
	
	@AgentArgument
	protected int argTimePickArticle;
	
	@AgentArgument
	protected int argTimeDeliverPickingOrder;
	
	@AgentArgument
	protected int argCapacity;
	
	@AgentArgument
	protected int argDelayEmergencyCall;
	
	@AgentArgument
	protected String argPathDarknet;
	
	@AgentArgument
	protected String argPathData;
	
	@AgentArgument
	protected String argPathConfig;
	
	@AgentArgument
	protected String argPathWeights;

	@AgentArgument
	protected double argThresholdObjectDetection;
	
	@AgentArgument
	protected String argPathYoloMark;
	
	@AgentArgument
	protected int argSpeedFactor;
	
	@AgentArgument
	protected int argTimeEvaluateSituation;
	
	@AgentArgument
	protected int argTimeModifyEnvironment;
	
	@AgentArgument
	protected int argTimeConfirmNewRecord;
	
	@AgentArgument
	protected int argTimeConfirmRelease;
	
	@AgentArgument
	protected int argTimeMarkArticle;
	
	@AgentArgument
	protected String argType;
	
	@AgentArgument
	protected int argMaxSizeOrderList;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess Agent;
	
	protected String AgentName;
	
	protected double MovementSpeed;	
	protected int TimePickArticle;
	protected int TimeDeliverPickingOrder;
	protected int DelayEmergencyCall;
	protected int Capacity;
	
	EmergencyCallWindow window;
	
	protected String PathDarknet;
	protected String PathData;
	protected String PathConfig;
	protected String PathWeights;	
	protected double ThresholdObjectDetection;
	protected String PathYoloMark;
	
	protected int SpeedFactor;
	protected int MaxSizeOrderList;
	
	protected IMonitorService MonitorService;
	
	final static int IMAGE_SIZE = 600;
	
	protected Random r = new Random();
	
	protected int TimeEvaluateSituation = 0;
	protected int TimeModifyEnvironment = 0;
	protected int TimeConfirmNewRecord = 0;
	protected int TimeConfirmRelease = 0;
	protected int TimeMarkArticle = 0;
	
	//-------- beliefs ---------
	
	@Belief
	protected List<Article> Articles = new ArrayList<Article>();
	
	@Belief
	protected List<PickingOrder> PickingOrders = new ArrayList<PickingOrder>();
	
	@Belief
	protected Warehouse Warehouse;
	
	@Belief
	protected WayPoint CurrentWayPoint;
	
	@Belief
	protected int FinishedPickingOrders = 0;
	
	@Belief
	protected boolean ControlSwitch = false;
	
	@Belief
	protected int TriggerdEmergencyCalls = 0;
	
	@Belief
	protected boolean Simulation = false;

	@Belief
	protected String Type;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		// set speed factor
		SpeedFactor = argSpeedFactor;
		
		// create warehouse (class Warehouse  couldn't successfully realized as serializable, therefor local instance)
		WarehouseFactory WF = new WarehouseFactory();
		Warehouse = WF.CreateWarehouse();
		
		// load arguments
		MovementSpeed = argMovementSpeed;
		TimePickArticle = argTimePickArticle;
		TimeDeliverPickingOrder = argTimeDeliverPickingOrder;
		Capacity = argCapacity;
		DelayEmergencyCall = argDelayEmergencyCall;
		
		PathDarknet = argPathDarknet;
		PathData = argPathData;
		PathConfig = argPathConfig;
		PathWeights = argPathWeights;
		ThresholdObjectDetection = argThresholdObjectDetection;
		PathYoloMark = argPathYoloMark;
		
		TimeEvaluateSituation = argTimeEvaluateSituation;
		TimeModifyEnvironment = argTimeModifyEnvironment;
		TimeConfirmNewRecord = argTimeConfirmNewRecord;
		TimeConfirmRelease = argTimeConfirmRelease;
		TimeMarkArticle = argTimeMarkArticle;

		MaxSizeOrderList = argMaxSizeOrderList;
		Type = argType;
		
		Articles.addAll(argArticles);		
		CurrentWayPoint = Warehouse.getTransferPlaces().get(argWayPoint);
		
		// read local name
		AgentName = Agent.getComponentIdentifier().getLocalName();
		
		// set monitoring service and register
		MonitorService = (IMonitorService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("MonitorService").get();
		MonitorService.Register(AgentName);
		
		// adopt global goal
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new FinishPickingOrders());
		
		// create GUI
		EventQueue.invokeLater(new Runnable() {	
			public void run() {
				// create window object
				window = new EmergencyCallWindow();
				// add action and event listeners
				try {
					ConfigureWindow();
				} catch (IOException e) {
					e.printStackTrace(); 
				}
			}
		});
				
		System.out.println("RobotAgent " + AgentName + " gestartet.");
		
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal(unique=true)
	public class TriggerControlSwitch {
		
		@GoalParameter
		protected int time;
		
		public TriggerControlSwitch(int time) {
			this.time = time*1000;
		}
		
		public int getTime() {
			return this.time;
		}
		
	}
	
	@Goal(recur=true)
	public class WaitForFeedback {
		
		@GoalRecurCondition(beliefs="ControlSwitch")
		public boolean checkRecur() {
		  return true;
		}		

		boolean value;
		
		public WaitForFeedback(boolean value) {
			this.value = value;
		}
		
		public WaitForFeedback(boolean value, int time) {
			this.value = value;
		}
		
		@GoalResult
		protected boolean getResult() {
			return true;
		}
		
		@GoalTargetCondition(beliefs={"ControlSwitch"})
		public boolean checkTarget() {
			return ControlSwitch != this.value;
		}
	
	}
	
	@Goal(unique=true)
	public class DetectObject {
		
		@GoalParameter
		protected Double Propability = 0.0;
		
		protected Article Article;
		protected String Image;
		protected String LastImage;
		protected String[] BoundingBox = new String[4];
		protected List<String> ImageList = new ArrayList<String>();	
		protected int EmergencyCall = 0;
		
		public DetectObject(Article article) {
			this.Article = article;	
			File folder = new File(article.getPathFolderImage());	
			File[] images = folder.listFiles();		
			for (int i=0; i<images.length; i++)
				ImageList.add(images[i].getAbsolutePath());
			this.LastImage = images[images.length-1].getAbsolutePath();
			selectImage();
		}
		
		public Article getArticle() {
			return this.Article;
		}
		
		public String getImage() {
			return this.Image;
		}
		
		public String getLastImage() {
			return this.LastImage;
		}
		
		public void selectImage() {
			if (ImageList.size() != 0) {
				int min = 0;
				int max = ImageList.size()-1;
				int value = r.nextInt((max - min) + 1) + min;
				this.Image = ImageList.get(value);
				ImageList.remove(value);
			} else {
				this.Image = "";
			}
		}
			
		public void setEmergencyCall(int level) {
			this.EmergencyCall = level;
		}
		
		public int getEmergencyCall() {
			return this.EmergencyCall;
		}
		
		public void setBoundingBox(String left, String top, String right, String bottom) {
			BoundingBox[0] = left;
			BoundingBox[1] = top;
			BoundingBox[2] = right;
			BoundingBox[3] = bottom;
		}
		
		public void setPropability(double value) {
			this.Propability = value;
		}
	
		@GoalResult
		protected double getResult() {
			return Propability;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return (Propability >= ThresholdObjectDetection);
		}
				
	}
	
	@Goal(excludemode=ExcludeMode.Never, unique=true) 
	public class FinishPickingOrders {
		
		@GoalTargetCondition(beliefs={"PickingOrders"})
		public boolean checkTarget() {
			return PickingOrders.size() == 0;
		}
		
		@GoalMaintainCondition(beliefs={"PickingOrders"})
		public boolean checkMaintain() {
			return PickingOrders.size() == 0;
		}
	
	}
	
	@Goal(retry=true) 
	public class MakeProposal {
		
		@GoalParameter
		protected int proposal = -1;
		
		protected PickingOrder order;
	
		public MakeProposal(PickingOrder order) {
			this.order = order;
		}
		
		public PickingOrder getPickingOrder() {
			return this.order;
		}
		
		public void setProposal(int value) {
			this.proposal = value;
		}
		
		@GoalResult
		protected int getResult() {
			return proposal;
		}
		
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return proposal > -1;
		}
			
	}
	
	@Goal(retry=true, recur=true, recurdelay=1000) 
	public class HumanSupport {
		
		@GoalParameter
		protected IHumanPickerService HumanPickerService = null;
	
		public HumanSupport() {
			
		}
				
		public void setHumanPickerService(IHumanPickerService service) {
			this.HumanPickerService = service;
		}
		
		@GoalResult
		protected IHumanPickerService getResult() {
			return this.HumanPickerService;
		}
		
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return this.HumanPickerService != null;
		}
			
	}
	
	//-------- plans ----------

	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=10)
	protected void UseYOLO(DetectObject goal) throws InterruptedException {
		
		List<Tuple2<Double, String[]>> ResultDetection = AlgorithmYOLO(goal.getImage(), goal.getArticle().getName());
		
		double prop = 0.0;
		String[] box = new String[4];
		
		// get article with highest propability of object detecetion
		for (Tuple2<Double, String[]> object : ResultDetection)		
			if (object.getFirstEntity() > prop) {
				prop = object.getFirstEntity();
				box = object.getSecondEntity();
			}

		goal.setBoundingBox(box[0], box[1], box[2], box[3]);
		goal.setPropability(prop);
		MonitorService.Status(AgentName, AgentName + ": Result of object detection -> " + String.valueOf(prop) + "%");
		
	}
		
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=8)
	protected void ChangeCameraAngle(DetectObject goal) throws InterruptedException {
				

		/* Hier sollte ein Bild via Photoneo mit ver�ndertem Kamera-Winkel aufgenommen, gespeichert und der Pfad weitergeleitet werden.
	     * Aktuell: Auswahl der n�chsten Bild-Datei. **
		 */
		
		MonitorService.Status(AgentName, AgentName + ": Adjustment Phase (change camera angle) gestartet.");
		
		//while (!goal.getImage().equals("")) {
		
		for (int i=0; i<2; i++) {

				List<Tuple2<Double, String[]>> ResultDetection = AlgorithmYOLO(goal.getImage(), goal.getArticle().getName());
				
				double prop = 0.0;
				String[] box = new String[4];
				
				// get article with highest propability of object detecetion
				for (Tuple2<Double, String[]> object : ResultDetection) {	
					if (object.getFirstEntity() > prop) {
						prop = object.getFirstEntity();
						box = object.getSecondEntity();
					}				
				}
				
				MonitorService.Status(AgentName, AgentName + ": Result of object detection -> " + String.valueOf(prop) + "%");
				
				if (prop >= ThresholdObjectDetection) {			
					goal.setBoundingBox(box[0], box[1], box[2], box[3]);
					goal.setPropability(prop);
					break;
				}
				
				goal.selectImage();
				
		}

	}
			
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=6)
	protected void SupportLevelOne(DetectObject goal, IPlan plan) throws InterruptedException, IOException {
				
		// support level 1: call human support and adjust environment
		
		MonitorService.Status(AgentName, AgentName + ": Emergency Call (support level 1) gestartet.");
		
		// show gui
		window.lblArticle.setText(goal.getArticle().getName());
	    window.frmEmergencyCall.setVisible(true);
	    
		window.taStatus.append("Start Support Level 1: \r\n");

		// search for human picker
		window.taStatus.append("Search for human picker");		
		IHumanPickerService service = (IHumanPickerService) plan.dispatchSubgoal(new HumanSupport()).get();
		window.taStatus.append(" - DONE \r\n");

		// import and show current view
		window.taStatus.append("Record picture");
		String path = goal.getLastImage();			
		File img = new File(path);
		BufferedImage bufferedImage = null;
		bufferedImage = ImageIO.read(img);	
		try {
			BufferedImage resizedicon = Thumbnails.of(bufferedImage).size(IMAGE_SIZE,IMAGE_SIZE).asBufferedImage();
			ImageIcon icon = new ImageIcon(resizedicon);
			window.lblPicture.setIcon(icon);			
		} catch (IOException e) {
			e.printStackTrace();
		}	
		window.taStatus.append(" - DONE \r\n");
		
		// assign support request and wait for arrival (return of assignment)
		service.AssignSupportOrder(CurrentWayPoint.getName(), AgentName);
		window.taStatus.append("Wait for human agent to arrive");
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		window.taStatus.append(" - OK \r\n");
		window.btnEnvironmentModified.setEnabled(true);
		
		// wait for adjustment
		window.taStatus.append("Wait for adjustment of environment");
		if (Simulation==true) plan.dispatchSubgoal(new TriggerControlSwitch(TimeEvaluateSituation + TimeModifyEnvironment));
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		window.btnEnvironmentModified.setEnabled(false);
		window.taStatus.append(" - OK \r\n");	
		
		// import and show current view		
		window.taStatus.append("Record picture");
		path = goal.getLastImage();		
		img = new File(path);
		bufferedImage = ImageIO.read(img);
		try {
			BufferedImage resizedicon = Thumbnails.of(bufferedImage).size(IMAGE_SIZE,IMAGE_SIZE).asBufferedImage();
			ImageIcon icon = new ImageIcon(resizedicon);
			window.lblPicture.setIcon(icon);			
		} catch (IOException e) {
			e.printStackTrace();
		}	
				
		window.taStatus.append(" - DONE \r\n");
		window.btnRetryObjectDetection.setEnabled(true);
		
		// wait for confirmation to restart object detection
		window.taStatus.append("Wait for trigger");
		if (Simulation==true) plan.dispatchSubgoal(new TriggerControlSwitch(TimeConfirmNewRecord));
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		window.btnRetryObjectDetection.setEnabled(false);
		window.taStatus.append(" - OK \r\n");
			
		// restart object detection by leaving current plan to retry goal
		window.taStatus.append("Restart object detection");
		
		double prop = 0.0;
		
		List<Tuple2<Double, String[]>> ResultDetection = null;
		ResultDetection = AlgorithmYOLO(goal.getImage(), goal.getArticle().getName());			
		String[] box = null;		
		// get article with highest propability of object detecetion
		for (Tuple2<Double, String[]> object : ResultDetection)		
			if (object.getFirstEntity() > prop) {
				prop = object.getFirstEntity();
				box = object.getSecondEntity();
			}
				
		if (prop >= ThresholdObjectDetection) {
			// Hier m�sste das Bild mit Annotation an den MySQL-Server geschickt bzw. lokal gespeichert werden.
			window.taStatus.append(" - OK \r\n");
			window.btnReleaseRobot.setEnabled(true);
			if (Simulation==true) plan.dispatchSubgoal(new TriggerControlSwitch(TimeConfirmRelease));
			plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
			window.btnReleaseRobot.setEnabled(false);
			// reset window
			window.taStatus.setText("");
			window.lblArticle.setText("");
			window.lblPicture.setIcon(null);
			window.frmEmergencyCall.setVisible(false);
		}
		
		goal.setBoundingBox(box[0], box[1], box[2], box[3]);
		goal.setPropability(prop);
						
	}
	
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=4)
	protected void SupportLevelTwo(DetectObject goal, IPlan plan) throws IOException, InterruptedException {
		
		// support level 2: human support marks object
		
		MonitorService.Status(AgentName, AgentName + ": Emergency Call (support level 2) gestartet.");
		
		window.taStatus.append(" - FAILED \r\n");
		window.taStatus.append("\r\n");
		window.taStatus.append("Start Support Level 2: \r\n");

		window.taStatus.append("Start YOLO Mark");	
				
		if (Simulation==false) {
			
			// create new directory for Emergency Call
			TriggerdEmergencyCalls++;
			File Dir = new File(PathYoloMark + "\\EC_" + AgentName + "_" + String.valueOf(TriggerdEmergencyCalls));				
			Dir.mkdir();
			// create new folder img with current picture
			File ImgDir = new File(Dir + "\\img");
			ImgDir.mkdir();
			Path sourceDir = Paths.get(goal.getLastImage());
			Path targetDir = Paths.get(ImgDir + "\\image.png");
			Files.copy(sourceDir, targetDir);
			// create new train.txt
			File train = new File(Dir + "\\train.txt");
			train.createNewFile();
			// create new obj.names
			File names = new File(Dir + "\\obj.names");
			names.createNewFile();
			BufferedWriter bw = new BufferedWriter(new FileWriter(Dir + "\\obj.names"));
			bw.write(goal.getArticle().getName());
			bw.close();
	
			// start of yolo_mark.exe (OpenCV)
			CommandShell cs = new CommandShell("txt_filename_path"); // <-- Das funktioniert, weil nur ein einziges Bild mit Yolo Mark annotiert wird!
	
			String cmdChangeDirectory = "cd " + PathYoloMark;
			cs.processInput(cmdChangeDirectory);
			
			String cmdObjectAnnotation = "yolo_mark.exe " + Dir + "\\img " + Dir + "\\train.txt " + Dir + "\\obj.names";
			cs.processInput(cmdObjectAnnotation);
			
			window.taStatus.append(" - DONE \r\n");
			window.taStatus.append("Wait for bounding box");
				
			cs.process.waitFor();
			
			/**
			Integer[] box = new Integer[4];		
				// Ausgabe CMD:
				// File opened for output: data/train.txt
				// File loaded: data/obj.names
				// "trackbar_value = 0"
				//	txt_filename_path = data/img/<name bild>.txt	
				// Eigentlich m�sste hier die Zeile text_filename_path ausgewertet werde.
				// Die aktuelle Version speichert aber immer nur eine Text-Datei, die im Ordner ImDir liegt.
			String file = ImgDir + "\\image.txt";
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null) {		
				String[] param = line.split(" ");
				for (int i=0; i<4; i++) {
					box[i] = Integer.valueOf(param[i+1]); // <-- Das erste Feld ist die Objekt-Klasse, d.h. hier "0"
				}	
			}
			br.close();
			goal.setBoundingBox(box[0], box[1], box[2], box[3]);	
			**/
			
			cs.dispose();
			
		} else {
			
			window.taStatus.append(" - DONE \r\n");
			window.taStatus.append("Wait for bounding box");
			
		}
		
		window.taStatus.append(" - OK \r\n");		
		// Hier k�nnte das Bild mit Annotation an den MySQL-Server geschickt werden.
		window.btnReleaseRobot.setEnabled(true);
		if (Simulation==true) plan.dispatchSubgoal(new TriggerControlSwitch(TimeMarkArticle + TimeConfirmRelease));
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		window.btnReleaseRobot.setEnabled(false);
		
		// reset window
		window.taStatus.setText("");
		window.lblArticle.setText("");
		window.lblPicture.setIcon(null);
		window.frmEmergencyCall.setVisible(false);

		goal.setPropability(100.0);

	}
	
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=2)
	protected void SupportLevelThree(DetectObject goal) throws IOException {
		
		// support level 3: pick object
		
		MonitorService.Status(AgentName, AgentName + ": Emergency Call (support level 3) gestartet.");
		
		/* Die letzte Support-Stufe wird nur relevant, wenn das Greifen durch eine separate Funktion gesteuert wird:
		 * W�re das Greifen durch die Markierung in der zweiten Stufe nicht m�glich, muss der menschliche Kommissionierer den Auftrag kommissionieren.
		 * Da das Greifen aktuell immer erfolgreich ist, wird diese Stufe ignoriert.
		 */
		
	}
	
	@Plan(trigger=@Trigger(goals=HumanSupport.class))
	protected void SearchHumanPicker(HumanSupport goal) {
			
		// call human support: start "auction" 
		IHumanPickerService[] HumanPickerServices = Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("HumanPickerService").get().toArray(new IHumanPickerService[0]);	
		
	    // create listener object
	    Future<Collection<Tuple2<IHumanPickerService, Integer>>> cfp =
	        new Future<Collection<Tuple2<IHumanPickerService, Integer>>>();        
	    final CollectionResultListener<Tuple2<IHumanPickerService, Integer>> crl =
	        new CollectionResultListener<Tuple2<IHumanPickerService, Integer>>(
	        		HumanPickerServices.length, false, new DelegationResultListener<Collection<Tuple2<IHumanPickerService, Integer>>>(cfp)
	        );
	    
	    // call service provider
	    for(int i=0; i<HumanPickerServices.length; i++) {     
	        IHumanPickerService HumanPickerService = HumanPickerServices[i];          
	        HumanPickerService.RequestSupport(CurrentWayPoint.getName()).addResultListener(new IResultListener<Integer>() {		                
	        	public void resultAvailable(Integer wp) {
	        		crl.resultAvailable(new Tuple2<IHumanPickerService, Integer>(HumanPickerService, wp));    
	        	}    		                
	        	public void exceptionOccurred(Exception exception) {
	        		crl.exceptionOccurred(exception);
	        	}		                
	        });    		            
	    }
	    
	    // wait for response
	    @SuppressWarnings("unchecked")
		Tuple2<IHumanPickerService, Integer>[] responses = cfp.get().toArray(new Tuple2[0]);
	    
	    // evaluate responses
	    IHumanPickerService service = null;
	    int time = Integer.MAX_VALUE;
	    for (Tuple2<IHumanPickerService, Integer> response : responses) {
	    	if (response.getSecondEntity() < time) {
	    		service = response.getFirstEntity();
	    		time = response.getSecondEntity();
	    	}
	    }
	    
	    goal.setHumanPickerService(service);
		
	}
	
	@Plan(trigger=@Trigger(goals=MakeProposal.class))
    protected void EvaluatePickingOrder(MakeProposal goal) {
		
		PickingOrder order = goal.getPickingOrder();		
		WayPoint WP = CurrentWayPoint;
		
		// Berechnung von tFinish
		int tFinish = 0;
		for (PickingOrder pickingorder : PickingOrders) {			
			for (Tuple2<Article,Integer> position : pickingorder.getPositions()) {					
				// Berechnung der Wegdauer analog StartPicking
				WayPoint newWP = Warehouse.getShelfs().get(position.getFirstEntity().getShelf());
				tFinish  = tFinish  + (int) (Warehouse.getDistance(WP, newWP) / MovementSpeed);
				WP = newWP;
				// Berechung der Pick-Dauer analog StartPicking <-- Hier kann es zu einer Ungenauigkeit kommen, wenn der Auftrag schon begonnen wurde.
				tFinish  = tFinish + position.getSecondEntity() * TimePickArticle;
			}
			// Berechnung der abschlie�enden Ablieferung analog StartPicking
			WayPoint TP = Warehouse.getTransferPlaces().get(order.getTransferPlace());
			tFinish  = tFinish + (int) (Warehouse.getDistance(WP, TP) / MovementSpeed);
			WP = TP;
			tFinish  = tFinish + TimeDeliverPickingOrder;
		}
			
		// Berechnung von tMovement
		int tMovement = 0;
		// Berechnung der Wegdauer zur ersten Position in "order" analog StartPicking
		WayPoint newWP = Warehouse.getShelfs().get(order.getPositions().get(0).getFirstEntity().getShelf());
		tMovement  = tMovement  + (int) (Warehouse.getDistance(WP, newWP) / MovementSpeed);
		
		// Berechnung von tEmergencyCall
		int tEmergencyCall = 0;
		for (Tuple2<Article,Integer> position : order.getPositions()) {		
			int PicksArticle = position.getSecondEntity();
			double PEOD = (100.0 - position.getFirstEntity().getPOD()) / 100;
			tEmergencyCall = tEmergencyCall + (int) (PicksArticle * DelayEmergencyCall * PEOD);
		}

		goal.setProposal(tFinish + tMovement + tEmergencyCall);
			
	}
	
	@Plan(trigger=@Trigger(goals=FinishPickingOrders.class))
	public class Picking {
	
		@PlanAPI
		RPlan plan;
		
		@PlanPrecondition
		protected Boolean PlanPrecondition() {
		    return PickingOrders.size() != 0;
		}

		@PlanBody
		public void Body() {
			
			PickingOrder order = PickingOrders.get(0);
			
			MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " gestartet.");
			
			for (Tuple2<Article,Integer> position : order.getPositions()) {
				
				// move to shelf
				WayPoint DestinationWayPoint = Warehouse.getShelfs().get(
						position.getFirstEntity().getShelf()
				);
				int tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, DestinationWayPoint) / MovementSpeed);
				plan.waitFor(tMovement*(1000/SpeedFactor)).get();			
				CurrentWayPoint = DestinationWayPoint;
				MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " erreicht.");

				for (int i=0; i<position.getSecondEntity(); i++) {
				
					// record picture and detect article(s) by creating sub-goal
					/* Hier sollte ein Bild via Photoneo aufgenommen, gespeichert und der Pfad weitergeleitet werden.
					 * Aktuell: Im Ziel wurden Aufnahmen importiert und durch selectImage wird eine davon ausgew�hlt.
					 */
					DetectObject goal = new DetectObject(position.getFirstEntity());
					plan.dispatchSubgoal(goal).get();
										
					// pick article(s)
					int tPicking = TimePickArticle;
					plan.waitFor(tPicking*(1000/SpeedFactor)).get();
					
					/* Hier kann das folgende Problem auftreten:
					 * Nach einem Emergency Call (support level 2) wird das Objekt markiert, das Ziel dadurch erf�llt und der Roboter greift den Artikel.
					 * Die Chance, dass es f�r diesen Artikel zu einem weiteren Emergency Call (support level 2) kommt, ist aber relativ hoch.
					 * Der menschliche Kommissionierer ist aber schon auf dem R�ckweg... 
					 * Wenn die Quantity f�r den kritischen Artikel hoch ist, kann durch diesen Effekt das gesamte System f�r l�ngere Zeit blockiert werden!
					 * Daher ist die Quantity pro Position aktuell auf 1 gesetzt - mit eine Vervielf�ltigung der Position k�nnte eine h�here Quantity modelliert werden.
					 */	
				
				}
				
				//order.getPositions().remove(position);
				MonitorService.Status(AgentName, AgentName + ": Position (" + position.getSecondEntity() + "x " + position.getFirstEntity().getName() + ") wurde kommissioniert.");
	
			}
			
			// finish order			
			IWarehouseService service = (IWarehouseService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("WarehouseService").get();
			service.FinishPickingOrder(order.getOrderID());
			MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " abgeschlossen.");
			
			/**
			// move to transfer place after finishing 2 picking orders
			FinishedPickingOrders++;
			if (FinishedPickingOrders == Capacity) {
			
				WayPoint DestinationWayPoint = Warehouse.getTransferPlaces().get(
						order.getTransferPlace()
				);
				int tMovement = (int) (Warehouse.getDistance(CurrentWayPoint, DestinationWayPoint) / MovementSpeed);
				plan.waitFor(tMovement*(1000/SpeedFactor)).get();
				CurrentWayPoint = DestinationWayPoint;
				MonitorService.Status(AgentName, AgentName + ": Waypoint " + CurrentWayPoint.getName() + " erreicht.");
				
				// deliver picking order
				plan.waitFor(TimeDeliverPickingOrder*(1000/SpeedFactor)).get();
				MonitorService.Status(AgentName, AgentName + ": Auftr�ge wurden an �bergabepunkt abgeliefert.");
				FinishedPickingOrders = 0;
				
			}
			**/
			
			// remove order -> triggers next order
			PickingOrders.remove(order);
			
		}
		
	}

	@Plan(trigger=@Trigger(goals=TriggerControlSwitch.class))
    protected void WaitUntilTrigger(TriggerControlSwitch goal, IPlan plan) {
	
		plan.waitFor(goal.getTime()).get();
		
		SetControlSwitch();

	}
			
	//-------- methods --------
	
	public List<Tuple2<Double,String[]>> AlgorithmYOLO(String image, String article) throws InterruptedException {
		
		CommandShell cs = new CommandShell("End of Object Detection");

		String cmdChangeDirectory = "cd " + PathDarknet;
		cs.processInput(cmdChangeDirectory);
		
		String cmdObjectDetection = "darknet.exe detector test " + PathData + " " + PathConfig + " " + PathWeights + " -dont_show " + image;
		cs.processInput(cmdObjectDetection);
		
		cs.process.waitFor();
		
		List<Tuple2<Double, String[]>> result = new ArrayList<Tuple2<Double,String[]>>();
		
		double prop = 0.0;
		String[] box = new String[4];
		
		for (int i=0; i<cs.Output.size(); i++) {
			
			// line --> Eimer: 83%			
			if (cs.Output.get(i).contains(article)) {
				
				double value = Double.valueOf(cs.Output.get(i).split(":")[1].replace("%", ""));
				
				if (value > prop) {
					prop = value;
					// search for entry containing "Bounding Box"
					// line --> Bounding Box: Left=XXXX, Top=XXXX, Right=XXXX, Bottom=XXXX						
					for (int k=i; k<cs.Output.size(); k++) {					
						if (cs.Output.get(k).contains("Bounding Box")) {
							for (int j=0; j<4; j++) {
								box[j] = cs.Output.get(k).split(":")[1].split(",")[j].trim().split("=")[1];			
							}
						}
						
					}					
					
				}
				
				result.add(new Tuple2<Double,String[]>(prop, box));
	
			}	
			
		}
		
		cs.dispose();
		
		return result;
	
	}
	
	public void ConfigureWindow() throws IOException {
		
		window.frmEmergencyCall.setTitle("Emergency Call - " + AgentName);
		
		window.btnEnvironmentModified.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();				
			}
		});
		
		window.btnRetryObjectDetection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();	
			}
		});

		window.btnReleaseRobot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();
			}
		});
		
	}
		
	public void SetControlSwitch() {
				
		if (ControlSwitch == true) 
			ControlSwitch = false;	
	    else 
	    	ControlSwitch = true;		
	}
	
	//-------- services --------
	
	public IFuture<Boolean> AssignPickingOrder(PickingOrder order) {

		final Future<Boolean> value = new Future<Boolean>();
		
		PickingOrders.add(order);
		//MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " angenommen.");
		
		value.setResult(true);			
		return value;

	}

	public IFuture<Integer> CallForProposal(PickingOrder order) {

		final Future<Integer> result = new Future<Integer>();
		
		int value = Integer.MAX_VALUE;
		
		if (window.frmEmergencyCall.isVisible() == false && PickingOrders.size() <= MaxSizeOrderList)
			value = (int) Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new MakeProposal(order)).get();
		
		// MonitorService.Status(AgentName, AgentName + ": Auftrag " + order.getOrderID() + " wurde bewertet: " + String.valueOf(value));
		
		result.setResult(value);
		return result;
		
	}
	
	public IFuture<WayPoint> RequestLocation() {
		
		final Future<WayPoint> result = new Future<WayPoint>();

		result.setResult(CurrentWayPoint);				
		return result;
				
	}
	
	public IFuture<Boolean> EmergencyCall() {
		
		final Future<Boolean> value = new Future<Boolean>();
		
		SetControlSwitch(); // human picker arrived

		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new WaitForFeedback(ControlSwitch)).get(); // environment modified		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new WaitForFeedback(ControlSwitch)).get(); // retry object detection		
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new WaitForFeedback(ControlSwitch)).get(); // release robot

		value.setResult(true);			
		return value;
		
	}
	
	public IFuture<String> RequestName() {

		final Future<String> result = new Future<String>();
		
		result.setResult(AgentName);				
		return result;
		
	}
	
	public IFuture<String> RequestType() {
		
		final Future<String> value = new Future<String>();

		value.setResult(Type);		
		return value;
		
	}
	
	public IFuture<Boolean> UpdatePOD(int id, double pod) {
		
		final Future<Boolean> value = new Future<Boolean>();
		
		for (Article article : Articles) {			
			if (article.getID() == id) {
				article.setPOD(pod);
				break;
			}			
		}
		
		value.setResult(true);		
		return value;
		
	}
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> UpdateCNN(String cnn) {
		
		final Future<Boolean> value = new Future<Boolean>();
		
		PathWeights = cnn; // Ich bin mir nicht sicher, ob das so funktioniert: Eventuell muss eine deep copy erstellt werden.
		
		value.setResult(true);		
		return value;
		
	}
	
}