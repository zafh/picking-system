package general;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {
	
	// ------- parameters -------
	
	final String Driver = "com.mysql.cj.jdbc.Driver";
	
	Connection connection = null;
	
	// ------- constructor -------
	
	public DatabaseManager() {	
		
        try { 
        	Class.forName(Driver).newInstance();
        	System.out.println("");
        	System.out.println("MySQL-Treiber geladen.");
        } 
        catch (Exception ex) {
        	ex.printStackTrace();
        }
		
	}
	
	// ------- methods -------
	
	public ResultSet executeSelect(String select) {
		
		Statement statement = null;
		ResultSet result = null;
		
		if (connection != null) {

			try {
			    statement = connection.createStatement();
			    result = statement.executeQuery(select);
			    System.out.println("SELECT-Statement erfolgreich.");
			}
			catch (SQLException ex){
				System.out.println("SELECT-Statement nicht erfolgreich!");
			    System.out.println("SQLException: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());
			}

		} else {
			System.out.println("");
			System.out.println("Es besteht keine Verbindung zu einem MySQL-Server!");
		}
		
		return result;
	}
	
	public boolean executeInsert(String insert) {
		
		Statement statement = null;
		
		if (connection != null) {

			try {
			    statement = connection.createStatement();
			    statement.executeUpdate(insert);
			    System.out.println("INSERT-Statement erfolgreich.");
			    return true;
			}
			catch (SQLException ex){
				System.out.println("INSERT-Statement nicht erfolgreich.");
			    System.out.println("SQLException: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());
			    return false;
			}
			
		} else {
			System.out.println("");
			System.out.println("Es wurde keine Verbindung zu einem MySQL-Server hergestellt!");
			return false;
		}
		
	}
	
	public boolean executeUpdate(String update) {
		
		Statement statement = null;
		
		if (connection != null) {

			try {
			    statement = connection.createStatement();
			    statement.executeUpdate(update);
			    System.out.println("UPDATE-Statement erfolgreich.");
			    return true;
			}
			catch (SQLException ex){
				System.out.println("UPDATE-Statement nicht erfolgreich.");
			    System.out.println("SQLException: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());
			    return false;
			}
			
		} else {
			System.out.println("");
			System.out.println("Es wurde keine Verbindung zu einem MySQL-Server hergestellt!");
			return false;
		}
		
	}
	
	
	public boolean connectDatabase(String Server, String Database, String User, String Password) {		
		
		try {
			String URL = "jdbc:mysql://" + Server + "/" + Database + "?autoReconnect=true&useSSL=true";
			connection = DriverManager.getConnection(URL, User, Password);
			System.out.println("Verbindung zum MySQL-Server (" + Server + ") hergestellt.");
			System.out.println("");
			return true;
		} 
		catch (SQLException ex) {
			System.out.println("Es konnte keine Verbindung zum MySQL-Server (" + Server + ") hergestellt werden.");
			ex.printStackTrace();
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
			return false;
		}
		
	}
		
}
