package general;

import java.util.ArrayList;
import java.util.List;
import objects.Track;
import objects.Warehouse;
import objects.WayPoint;
import general.DPQ;
import jadex.commons.Tuple2;

public class WarehouseFactory {

	public WarehouseFactory() {
		
	}
	
	public Warehouse CreateWarehouse() {
		
		Warehouse Warehouse = new Warehouse();
		 
	    WayPoint WP1  = new WayPoint(0, "WP1" , 0, 4);
    		Warehouse.addWayPoint(WP1);
	    WayPoint WP2  = new WayPoint(1, "WP2" , 1, 4);
	   		Warehouse.addWayPoint(WP2);
	    WayPoint WP3  = new WayPoint(2, "WP3" , 2, 4);
			Warehouse.addWayPoint(WP3);
	    WayPoint WP4  = new WayPoint(3, "WP4" , 3, 4);
			Warehouse.addWayPoint(WP4);
	    WayPoint WP5  = new WayPoint(4, "WP5" , 4, 4);
			Warehouse.addWayPoint(WP5);
	    WayPoint WP6  = new WayPoint(5, "WP6" , 5, 4);
			Warehouse.addWayPoint(WP6);        
	    WayPoint WP7  = new WayPoint(6, "WP7" , 6, 4); 
			Warehouse.addWayPoint(WP7);        
	    WayPoint WP8  = new WayPoint(7, "WP8" , 6, 3);
			Warehouse.addWayPoint(WP8);        
	    WayPoint WP9  = new WayPoint(8, "WP9" , 0, 2);
			Warehouse.addWayPoint(WP9);        
	    WayPoint WP10 = new WayPoint(9, "WP10", 1, 2);
			Warehouse.addWayPoint(WP10);        
	    WayPoint WP11 = new WayPoint(10, "WP11", 2, 2);
			Warehouse.addWayPoint(WP11);      
	    WayPoint WP12 = new WayPoint(11, "WP12", 3, 2);
			Warehouse.addWayPoint(WP12);        
	    WayPoint WP13 = new WayPoint(12, "WP13", 4, 2);
			Warehouse.addWayPoint(WP13);        
	    WayPoint WP14 = new WayPoint(13, "WP14", 5, 2);
			Warehouse.addWayPoint(WP14);        
	    WayPoint WP15 = new WayPoint(14, "WP15", 6, 2); 
			Warehouse.addWayPoint(WP15);        
	    WayPoint WP16 = new WayPoint(15, "WP16", 6, 1); 
			Warehouse.addWayPoint(WP16);        
	    WayPoint WP17 = new WayPoint(16, "WP17", 0, 0);
			Warehouse.addWayPoint(WP17);        
	    WayPoint WP18 = new WayPoint(17, "WP18", 1, 0);
			Warehouse.addWayPoint(WP18);       
	    WayPoint WP19 = new WayPoint(18, "WP19", 2, 0);
			Warehouse.addWayPoint(WP19);       
	    WayPoint WP20 = new WayPoint(19, "WP20", 3, 0);
			Warehouse.addWayPoint(WP20);       
	    WayPoint WP21 = new WayPoint(20, "WP21", 4, 0);
			Warehouse.addWayPoint(WP21);        
	    WayPoint WP22 = new WayPoint(21, "WP22", 5, 0);
			Warehouse.addWayPoint(WP22);       
	    WayPoint WP23 = new WayPoint(22, "WP23", 6, 0);
	    	Warehouse.addWayPoint(WP23);   
	    	
	    Track T1_2 = new Track ("T1_2", WP1, WP2); 	
	    	Warehouse.addTrack(T1_2);       
	    Track T1_9 = new Track ("T1_9", WP1, WP9); 	
			Warehouse.addTrack(T1_9);	
	    Track T2_1 = new Track ("T2_1", WP2, WP1); 	
			Warehouse.addTrack(T2_1);
	    Track T2_3 = new Track ("T2_3", WP2, WP3); 	
			Warehouse.addTrack(T2_3);
	    Track T3_2 = new Track ("T3_2", WP3, WP2); 	
			Warehouse.addTrack(T3_2);
	    Track T3_4 = new Track ("T3_4", WP3, WP4); 	
			Warehouse.addTrack(T3_4);
	    Track T4_3 = new Track ("T4_3", WP4, WP3); 	
			Warehouse.addTrack(T4_3);
	    Track T4_5 = new Track ("T4_5", WP4, WP5); 	
			Warehouse.addTrack(T4_5);
	    Track T4_12 = new Track ("T4_12", WP4, WP12); 	
			Warehouse.addTrack(T4_12);
	    Track T5_4 = new Track ("T5_4", WP5, WP4); 	
			Warehouse.addTrack(T5_4);
	    Track T5_6 = new Track ("T5_6", WP5, WP6); 	
			Warehouse.addTrack(T5_6);
	    Track T6_7 = new Track ("T6_7", WP6, WP7); 	
			Warehouse.addTrack(T6_7);
	    Track T6_5 = new Track ("T6_5", WP6, WP5); 	
			Warehouse.addTrack(T6_5);
	    Track T7_6 = new Track ("T7_6", WP7, WP6); 	
			Warehouse.addTrack(T7_6);
	    Track T7_8 = new Track ("T7_8", WP7, WP8); 	
			Warehouse.addTrack(T7_8);
	    Track T8_7 = new Track ("T8_7", WP8, WP7); 	
			Warehouse.addTrack(T8_7);
	    Track T8_15 = new Track ("T8_15", WP8, WP15); 	
			Warehouse.addTrack(T8_15);
	    Track T9_1 = new Track ("T9_1", WP9, WP1); 	
			Warehouse.addTrack(T9_1);
	    Track T9_10 = new Track ("T9_10", WP9, WP10); 	
			Warehouse.addTrack(T9_10);
	    Track T9_17 = new Track ("T9_17", WP9, WP17); 	
			Warehouse.addTrack(T9_17);
	    Track T10_9 = new Track ("T10_9", WP10, WP9); 	
			Warehouse.addTrack(T10_9);
	    Track T10_11 = new Track ("T10_11", WP11, WP11); 	
			Warehouse.addTrack(T10_11);
	    Track T11_10 = new Track ("T11_10", WP11, WP10); 	
			Warehouse.addTrack(T11_10);
	    Track T11_12 = new Track ("T11_12", WP11, WP12); 	
			Warehouse.addTrack(T11_12);
	    Track T12_4 = new Track ("T12_4", WP12, WP4); 	
			Warehouse.addTrack(T12_4);
	    Track T12_11 = new Track ("T12_11", WP12, WP11); 	
			Warehouse.addTrack(T12_11);
	    Track T12_13 = new Track ("T12_13", WP12, WP13); 	
			Warehouse.addTrack(T12_13);
	    Track T13_12 = new Track ("T13_12", WP13, WP12); 	
			Warehouse.addTrack(T13_12);
	    Track T13_14 = new Track ("T13_14", WP13, WP14); 	
			Warehouse.addTrack(T13_14);
	    Track T14_13 = new Track ("T14_13", WP14, WP13); 	
			Warehouse.addTrack(T14_13);
	    Track T14_15 = new Track ("T14_15", WP14, WP15); 	
			Warehouse.addTrack(T14_15);
	    Track T15_8 = new Track ("T15_8", WP15, WP8); 	
			Warehouse.addTrack(T15_8);
	    Track T15_14 = new Track ("T15_14", WP15, WP14); 	
			Warehouse.addTrack(T15_14);
	    Track T15_16 = new Track ("T15_16", WP15, WP16); 	
			Warehouse.addTrack(T15_16);
	    Track T16_15 = new Track ("T16_15", WP16, WP15); 	
			Warehouse.addTrack(T16_15);
	    Track T16_23 = new Track ("T16_23", WP16, WP23); 	
			Warehouse.addTrack(T16_23);
	    Track T17_9 = new Track ("T17_9", WP17, WP9); 	
			Warehouse.addTrack(T17_9);
	    Track T17_18 = new Track ("T17_18", WP17, WP18); 	
			Warehouse.addTrack(T17_18);
	    Track T18_17 = new Track ("T18_17", WP18, WP17); 	
			Warehouse.addTrack(T18_17);
	    Track T18_19 = new Track ("T18_19", WP18, WP19); 	
			Warehouse.addTrack(T18_19);
	    Track T19_18 = new Track ("T19_18", WP19, WP18); 	
			Warehouse.addTrack(T19_18);
	    Track T19_20 = new Track ("T19_20", WP19, WP20); 	
			Warehouse.addTrack(T19_20);
	    Track T20_12 = new Track ("T20_12", WP20, WP12); 	
			Warehouse.addTrack(T20_12);
	    Track T20_19 = new Track ("T20_19", WP20, WP19); 	
			Warehouse.addTrack(T20_19);
	    Track T20_21 = new Track ("T20_21", WP20, WP21); 	
			Warehouse.addTrack(T20_21);        
		Track T21_20 = new Track ("T21_20", WP21, WP20); 	
			Warehouse.addTrack(T21_20);
	    Track T21_22 = new Track ("T21_22", WP21, WP22); 	
			Warehouse.addTrack(T21_22);
	    Track T22_21 = new Track ("T22_21", WP22, WP21); 	
			Warehouse.addTrack(T22_21);
	    Track T22_23 = new Track ("T22_23", WP22, WP23); 	
			Warehouse.addTrack(T22_23);
	    Track T23_22 = new Track ("T23_22", WP23, WP22); 	
			Warehouse.addTrack(T23_22);
	    Track T23_16 = new Track ("T23_16", WP23, WP16); 	
			Warehouse.addTrack(T23_16);
			
		Warehouse.addShelf("A1", WP2);
		Warehouse.addShelf("A2", WP3);
		Warehouse.addShelf("A3", WP4);
		Warehouse.addShelf("A4", WP5);
		Warehouse.addShelf("A5", WP6);
		Warehouse.addShelf("B1", WP2);
		Warehouse.addShelf("B2", WP3);
		Warehouse.addShelf("B3", WP10);
		Warehouse.addShelf("B4", WP11);
		Warehouse.addShelf("C1", WP5);
		Warehouse.addShelf("C2", WP6);
		Warehouse.addShelf("C3", WP13);
		Warehouse.addShelf("C4", WP14);
		Warehouse.addShelf("D1", WP10);
		Warehouse.addShelf("D2", WP11);
		Warehouse.addShelf("D3", WP18);
		Warehouse.addShelf("D4", WP19);
		Warehouse.addShelf("E1", WP13);
		Warehouse.addShelf("E2", WP14);
		Warehouse.addShelf("E3", WP21);
		Warehouse.addShelf("E4", WP22);
		Warehouse.addShelf("F1", WP18);
		Warehouse.addShelf("F2", WP19);
		Warehouse.addShelf("F3", WP20);
		Warehouse.addShelf("F4", WP21);
		Warehouse.addShelf("F5", WP22);
		
		Warehouse.addTransferPlace("TransferStation", WP8);

		// adjacency list representation of the connected edges
	    List<List<Node>> adj = new ArrayList<List<Node>>();

	    // initialize list for every node 
	    for (WayPoint WP : Warehouse.WayPoints) { 	    	 
	         List<Node> list = new ArrayList<Node>();	         
	         for (Tuple2<WayPoint,Integer> adjWP : WP.AdjacentWayPoints)      	 
	        	 list.add(new Node(adjWP.getFirstEntity().getID(), adjWP.getSecondEntity()));  // adj.get(0).add(new Node(1, 9)); --> distance node 0 zu node 1 = 9
	         adj.add(WP.getID(),list);
	    } 
	    
	    // calculate the shortest paths and create distance matrix
	    Warehouse.Distances = new int[Warehouse.WayPoints.size()][Warehouse.WayPoints.size()];
	    for (WayPoint WP : Warehouse.WayPoints) {
	    	 DPQ DPQ = new DPQ(Warehouse.WayPoints.size());	     
		     DPQ.dijkstra(adj, WP.ID);   		      
		     for (int i=0; i<DPQ.dist.length; i++) 		    	 
		         Warehouse.Distances[WP.ID][i] = DPQ.dist[i]; 
	    }

		return Warehouse;
		
	}

}
