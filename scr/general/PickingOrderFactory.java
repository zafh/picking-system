package general;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import objects.PickingOrder;
import objects.Article;

public class PickingOrderFactory {
	
	// variables
	protected int maxPosition = 0;
	protected int maxQuantity = 0;	
	protected List<Article> ListArticle = new ArrayList<Article>();
	protected String TransferPlace = "";	
	protected int OrderIndex = 1;
	protected List<PickingOrder> ListPickingOrder = new ArrayList<PickingOrder>();
	protected Random rP;
	protected Random rA;
	protected Random rQ;
	protected int[] Demand;
		
	// constructor
	public PickingOrderFactory() {
		rP = new Random();
		rA = new Random();
		rQ = new Random();
	}
	
	// create many picking orders
	public List<PickingOrder> generatePickingOrders(int number) {		
		List<PickingOrder> list = new ArrayList<PickingOrder>();		
		for (int i=0; i<number; i++)
			list.add(generatePickingOrder());
		return list;			
	}
	
	// create a picking order
	public PickingOrder generatePickingOrder() {
		PickingOrder order = new PickingOrder("Order_" + String.valueOf(OrderIndex));
		OrderIndex++;
		// create random number of positions		
		int Pos = rP.nextInt((maxPosition - 1) + 1) + 1;
		for (int p=0; p<Pos; p++ ) {
			// get random article
			int Art = rA.nextInt((ListArticle.size() - 1) + 1) + 1;
			Article article = ListArticle.get(Art-1);
			// get random quantity
			int Qua = rQ.nextInt((maxQuantity - 1) + 1) + 1;
			// add position
			order.addPosition(article, Qua);
		}	
		// set transfer place
		order.setTransferPlace(TransferPlace);
		return order;	
	}
	
	public void setMaxPosition(int value) {
		this.maxPosition = value;
	}
	
	public void setMaxQuantity(int value) {
		this.maxQuantity = value;
	}
	
	public void setArticles(List<Article> articles) {
		ListArticle.clear();
		for (Article article : articles)
			ListArticle.add(article);
	}
	
	public void setTransferPlace(String name) {
		this.TransferPlace = name;
	}
	
	public PickingOrder getPickingOrder() {	
		if (ListPickingOrder.isEmpty())
			return null;		
		PickingOrder order = ListPickingOrder.get(0);
		ListPickingOrder.remove(0);
		return order;		
	}
	
	public List<PickingOrder> getListPickingOrder() {
		return this.ListPickingOrder;
	}
	
	public void importPickingOrders(String path) throws IOException {		
		ListPickingOrder.clear();
		Demand = new int[ListArticle.size()];
		BufferedReader br = new BufferedReader( new FileReader(path));			
		String line;
		while((line = br.readLine()) != null) {		
			String[] param = line.split(";");	
			// OrderID
			PickingOrder order = new PickingOrder (param[0].trim()); 
			// TransferPlace
			order.setTransferPlace(param[1].trim()); 
			// Positions (Article+Quantity)		
			for (int i=2; i<param.length; i++) {
				Article article = null;
				for (Article art : ListArticle) {
					if (art.getName().equals(param[i].trim())) {
						article = art;					
						Demand[ListArticle.indexOf(article)]++;
					}
				}			
				i++;		
				order.addPosition(article, Integer.valueOf(param[i]));			
			}
			ListPickingOrder.add(order);
		}
		br.close();	
	}
	
	public int[] getDemand() {

		return Demand;

	}
		
}