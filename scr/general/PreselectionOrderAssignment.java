package general;

import java.util.Arrays;
import ilog.concert.IloException;
import ilog.cp.IloCP;
import ilog.opl.*;

public class PreselectionOrderAssignment {
	
	public static <OplDecisionExprSolutionCallback> double CalculateThreshold(int WT, double L_EC_H, double L_EC_R, double C_H, double C_R, int numArticle, int[] D_F) throws IloException {

		double result = 100.0;
		
		try {		 	

			IloOplFactory.setDebugMode(false);
			IloOplFactory factoryOPL = new IloOplFactory();
			
			IloOplModelSource modelSource = factoryOPL.createOplModelSource("CalculationSubsetRobot_POD.mod");		
			IloOplErrorHandler errHandler = factoryOPL.createOplErrorHandler();		
			IloOplSettings set = factoryOPL.createOplSettings(errHandler);
			IloOplModelDefinition def = factoryOPL.createOplModelDefinition(modelSource, set);			
			IloCP cp = factoryOPL.createCP();	
			IloOplModel model = factoryOPL.createOplModel(def, cp);		
			
			IloOplDataSource dataWT = factoryOPL.createOplDataSourceFromString("WT = " + WT + ";", "WT");
			model.addDataSource(dataWT);			
			IloOplDataSource dataL_EC_H = factoryOPL.createOplDataSourceFromString("L_EC_H = " + L_EC_H + ";", "L_EC_H");
			model.addDataSource(dataL_EC_H);
			IloOplDataSource dataL_EC_R = factoryOPL.createOplDataSourceFromString("L_EC_R = " + L_EC_R + ";", "L_EC_R");
			model.addDataSource(dataL_EC_R);			
			IloOplDataSource dataC_H = factoryOPL.createOplDataSourceFromString("C_H = " + C_H + ";", "C_H");
			model.addDataSource(dataC_H);
			IloOplDataSource dataC_R = factoryOPL.createOplDataSourceFromString("C_R = " + C_R + ";", "C_R");
			model.addDataSource(dataC_R);
			IloOplDataSource datanumArticle = factoryOPL.createOplDataSourceFromString("numArticle=" + numArticle + ";", "numArticle");
			model.addDataSource(datanumArticle);		
			IloOplDataSource dataD_F = factoryOPL.createOplDataSourceFromString("D_F=" + Arrays.toString(D_F) + ";", "D_F");
			model.addDataSource(dataD_F);
			
			model.generate();

			cp.setOut(null);
			if (cp.solve()){
				
				model.postProcess();			
				result = model.getElement("POD").asNum();
				cp.end();
				
			}
			
			else {

			}
						
		}	
				
		catch (IloOplException ex) {
			System.err.println("### OPL exception: " + ex.getMessage());
			ex.printStackTrace();
		} 
		
		catch (IloException ex) {
			System.err.println("### CONCERT exception: " + ex.getMessage());
			ex.printStackTrace();			
		}	
		
		return result;
		
	}	
		
}