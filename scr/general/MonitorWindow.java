package general;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;

public class MonitorWindow {

	//-------- window objects --------
	
	public JFrame frmMonitor;
	
	//-------- constructor --------
	
	public MonitorWindow() {
		initialize();
	}
	
	//-------- attributes --------
	
	public JTextArea taStatus; 
	public JTabbedPane TabMenu;

	//-------- methods --------
	
	private void initialize() {
		
		frmMonitor = new JFrame();
		frmMonitor.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmMonitor.setTitle("Monitor Picking System");
		frmMonitor.setBounds(100, 100, 838, 529);
		
		JPanel StatusDisplay = new JPanel();
		frmMonitor.getContentPane().setLayout(new BorderLayout(0, 0));
		StatusDisplay.setLayout(new BorderLayout(0, 0));
		
		JScrollPane spStatus = new JScrollPane();
		StatusDisplay.add(spStatus, BorderLayout.CENTER);
		
		taStatus = new JTextArea();
		taStatus.setEditable(false);
		taStatus.setRows(3);
		taStatus.setForeground(Color.BLACK);
		spStatus.setViewportView(taStatus);
		frmMonitor.getContentPane().add(StatusDisplay, BorderLayout.SOUTH);
		
		TabMenu = new JTabbedPane(JTabbedPane.TOP);
		TabMenu.setName("");
		frmMonitor.getContentPane().add(TabMenu, BorderLayout.CENTER);

	}
}
