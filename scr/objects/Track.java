package objects;

import objects.WayPoint;

public class Track {
	
	public String Name;
	public WayPoint Start;
	public WayPoint End;
	public int Length;
	
	public Track (String name, WayPoint start, WayPoint end) {
		this.Name = name;
		this.Start = start;
		this.End = end;
		this.Length = GetLength(start, end);		
		start.addAdjecentWayPoint(end, this.Length);
	}
	
	public String getName() {
		return this.Name;
	}
	
	public WayPoint getStart() {
		return this.Start;
	}
	
	public WayPoint getEnd() {
		return this.End;
	}
	
	private int GetLength(WayPoint wp1, WayPoint wp2) {
		double x = Math.abs(wp1.getX() - wp2.getX());		
		double y = Math.abs(wp1.getY() - wp2.getY());
		return (int) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
}
