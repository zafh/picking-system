package objects;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.beans.PropertyChangeListener;

public class LearningSet implements Serializable {

	//-------- constants --------
	
	private static final long serialVersionUID = 1L;
	
	//-------- attributes --------

	protected List<String> Classes = new ArrayList<String>();		// List<Article>
	protected List<String> Data = new ArrayList<String>();			// List<Picture>
	
	protected List<String> TrainingSet = new ArrayList<String>();		// List<Picture>
	protected List<String> TestSet = new ArrayList<String>();			// List<Picture>
	
	public SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------
	
	public LearningSet() {
		
		this.pcs = new SimplePropertyChangeSupport(this);
		
	}
	
	//-------- methods --------
	
	public void CreateTrainingData(String path) throws IOException {	
		
		BufferedWriter bw;
		BufferedReader br;
		
		// create obj.data
		bw = new BufferedWriter( new FileWriter(path + "\\obj.data") );
		bw.write("classes = " + Classes.size());
		bw.newLine() ;
		bw.write("train = " + path + "data/" + "train.txt");
		bw.newLine() ;
		bw.write("valid = " + path + "data/" + "test.txt");
		bw.newLine() ;
		bw.write("names = " + path + "data/" + "obj.names");
		bw.newLine() ;
		bw.write("backup = " + path);
		bw.close();
		
		// create train.txt
		bw = new BufferedWriter( new FileWriter(path + "\\train.txt") );
		for (String pic : TrainingSet) {
			bw.write(pic);
			bw.newLine();
		}	
		bw.close();
				
		// create test.txt
		bw = new BufferedWriter( new FileWriter(path + "\\test.txt") );
		for (String pic : TestSet) {
			bw.write(pic);
			bw.newLine();
		}	
		bw.close();
		
		// create obj.names
		bw = new BufferedWriter( new FileWriter(path + "\\obj.names") );
		for (String art : Classes) {
			bw.write(art);
			bw.newLine();
		}	
		bw.close();
				
		// create yolo-obj.cfg
		bw = new BufferedWriter( new FileWriter(path + "\\yolo-obj.cfg") );
		br = new BufferedReader( new FileReader("yolo-obj.cfg"));	
		String line;
		while((line = br.readLine()) != null) {		
			if (line.length() >= 8)
				if (line.substring(0,7).equals("classes"))
					line = "classes=" + Classes.size();
			bw.write(line);
			bw.newLine();
		}
		bw.close();
		br.close();

	}
	
	// create training and test set according to data and rate 
	public void CreateSets(double ratio) {
		
		if (Data == null) return;
		
		TrainingSet.clear();
		TestSet.clear();
		
		for (String picture : Data) {
			
			double value = Math.random();
			
			if (value > ratio)
				TrainingSet.add(picture);
			else
				TestSet.add(picture);
			
		}	
		
	}

	//-------- property methods --------
	
	public synchronized List<String> getClasses() {
		return this.Classes;
	}
	public synchronized void setClasses(List<String> classes) {
		List<String> old = this.Classes;
		this.Classes = classes;
		pcs.firePropertyChange("Classes", old, classes);
	}
	public synchronized void addClass(String clazz) {
		List<String> old = this.Classes;
		this.Classes.add(clazz);
		pcs.firePropertyChange("Classes", old, clazz);
	}
	
	public synchronized List<String> getTrainingSet() {
		return this.TrainingSet;
	}
	
	public synchronized List<String> getTestSet() {
		return this.TestSet;
	}
	
	public synchronized List<String> getData() {
		return this.Data;
	}
	public synchronized void setData(List<String> data) {
		List<String> old = this.Data;
		this.Data = data;
		pcs.firePropertyChange("Data", old, data);
	}
	public synchronized void addPictureToData(String picture) {
		List<String> old = this.Data;
		this.Data.add(picture);
		pcs.firePropertyChange("Data", old, this.Data);	
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}
