package objects;

import java.io.Serializable;

import jadex.bridge.service.annotation.Reference;
import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.beans.PropertyChangeListener;

@Reference
public class Picture implements Serializable {

	//-------- constants --------

	private static final long serialVersionUID = 1L;
	
	public static final String StatusAnnotation = "Annotation";
	public static final String StatusTraining = "Training";
	public static final String StatusDetection = "Detection";
	
	//-------- attributes --------

	protected String PictureID;	
	protected int Article;	
	protected int Index;
	protected String Status;
	protected String Date;
	protected int Rotation;
	protected int Angle;
	protected String BoundingBox;
	protected String Datapath;
	protected String Camera;
	
	public SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------

	public Picture(String pictureid, int article, int index, String status, String date, int rotation, int angle, String boundingbox, String datapath, String camera) {
		
		this.PictureID = pictureid;
		this.Article = article;
		this.Index = index;
		this.Status = status;
		this.Date = date;
		this.Rotation = rotation;
		this.Angle = angle;
		this.BoundingBox = boundingbox;
		this.Datapath = datapath;
		this.Camera = camera;
		
		this.pcs = new SimplePropertyChangeSupport(this);	
		
	}
	
	//-------- methods --------

	public synchronized String getPictureID() {
		return this.PictureID;
	}
	
	public synchronized int getArticle() {
		return this.Article;
	}
	
	public synchronized int getIndex() {
		return this.Index;
	}
	
	public synchronized String getStatus() {
		return this.Status;
	}
	public synchronized void setStatus(String status) {
		String old = this.Status;
		this.Status = status;
		pcs.firePropertyChange("Status", old, status);
	}

	public synchronized String getDate() {
		return this.Date;
	}
	
	public synchronized int getRotation() {
		return this.Rotation;
	}
	
	public synchronized int getAngle() {
		return this.Angle;
	}
	
	public synchronized String getBoundingBox() {
		return this.BoundingBox;
	}
	public synchronized void setBoundingBox(String boundingbox) {
		String old = this.BoundingBox;
		this.BoundingBox = boundingbox;
		pcs.firePropertyChange("BoundingBox", old, boundingbox);
	}

	public synchronized String getDatapath() {
		return this.Datapath;
	}
	
	public synchronized String getCamera() {
		return this.Camera;
	}
	
	//-------- property methods --------

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}

