package objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jadex.commons.Tuple2;

public class WayPoint implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public int ID;
	public String Name;
	public int X;
	public int Y;
	public List<Tuple2<WayPoint,Integer>> AdjacentWayPoints = new ArrayList<Tuple2<WayPoint,Integer>>();
	
	public WayPoint(int id, String name, int x, int y) {
		this.ID = id;
		this.Name = name;
		this.X = x;
		this.Y = y;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public String getName() {
		return this.Name;
	}
	
	public int getX() {
		return this.X;
	}
		
	public int getY() {
		return this.Y;
	}
	
	public void addAdjecentWayPoint(WayPoint wp, int distance) {
		AdjacentWayPoints.add(new Tuple2<WayPoint,Integer>(wp, distance));
	}
	
}
