package objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Warehouse implements Serializable {

	/******************* variables *******************/
	private static final long serialVersionUID = 1L;

	public List<WayPoint> WayPoints;
	public List<Track> Tracks;
	public Map<String,WayPoint> Shelfs;
	public Map<String,WayPoint> TransferPlaces;	
	public int[][] Distances;
	
	/******************* constructor *******************/
	public Warehouse () {
		this.WayPoints = new ArrayList<WayPoint>();
		this.Tracks = new ArrayList<Track>();
		this.Shelfs = new HashMap<String,WayPoint>();
		this.TransferPlaces = new HashMap<String,WayPoint>();
	}
	
	/******************* data *******************/
	public List<WayPoint> getWayPoints() {
		return this.WayPoints;
	}
	
	public List<Track> getTracks() {
		return this.Tracks;
	}
	
	public Map<String,WayPoint> getShelfs() {
		return this.Shelfs;
	}
	
	public Map<String,WayPoint> getTransferPlaces() {
		return this.TransferPlaces;
	}
	
	/******************* methods *******************/	
	
	public WayPoint getWayPoint(String wp) {		
		int index = 0;
		for (WayPoint WP : WayPoints) {
			if (WP.getName().equals(wp)) {
				index = WayPoints.indexOf(WP);
				break;
			}
		}
		return WayPoints.get(index);
	}
	
	public int getDistance(WayPoint start, WayPoint end) {
		return Distances[start.getID()][end.getID()];
	}
	
	public void addWayPoint(WayPoint WayPoint) {
		WayPoints.add(WayPoint);
	}

	public void addTrack(Track track) {
		Tracks.add(track);
	}
	
	public void addShelf(String name, WayPoint waypoint) {
		Shelfs.put(name, waypoint);
	}
	
	public void addTransferPlace(String name, WayPoint waypoint) {
		TransferPlaces.put(name, waypoint);
	}
	
}
