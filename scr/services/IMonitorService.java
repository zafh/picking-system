package services;

import jadex.bridge.service.annotation.Timeout;

public interface IMonitorService {

	@Timeout(Timeout.NONE)
	public void Register(String name);
	
	@Timeout(Timeout.NONE)
	public void Status(String name, String status);
	
}
