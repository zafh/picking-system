package services;

import java.util.HashMap;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;
import objects.Article;

public interface IWarehouseService {

	@Timeout(Timeout.NONE)
	public IFuture<Boolean> FinishPickingOrder(String id);
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> UpdatePOD(int id, double pod);
	
	@Timeout(Timeout.NONE)
	public IFuture<HashMap<Article,Integer>> GetForecast();

}
