package services;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;
import objects.PickingOrder;

public interface IPickerService {

	@Timeout(Timeout.NONE)
	public IFuture<Boolean> AssignPickingOrder(PickingOrder order);
	
	@Timeout(10000)
	public IFuture<Integer> CallForProposal(PickingOrder order);
	
	@Timeout(Timeout.NONE)
	public IFuture<String> RequestType();
	
}
