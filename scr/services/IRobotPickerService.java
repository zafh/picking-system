package services;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IRobotPickerService {
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> EmergencyCall();

	@Timeout(Timeout.NONE)
	public IFuture<String> RequestName();
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> UpdatePOD(int id, double pod);
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> UpdateCNN(String cnn);
	
}
