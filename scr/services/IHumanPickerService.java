package services;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IHumanPickerService {
	
	@Timeout(Timeout.NONE)
	public IFuture<Integer> RequestSupport(String wp);
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> AssignSupportOrder(String wp, String robot);

}
