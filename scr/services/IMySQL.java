package services;

import java.util.HashMap;
import java.util.List;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IMySQL {

	@Timeout(Timeout.NONE)
	public IFuture<Boolean> executeUpdate(String update);
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> executeInsert(String insert);
	
	@Timeout(Timeout.NONE)
	public IFuture<List<HashMap<String,Object>>> executeSelect(String select);
	
}
