package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import objects.Article;

public class AgentFactoryConfig {

	protected int HumanAgent;
	protected int RobotAgent;

	protected List<Article> Articles = new ArrayList<Article>();
	
	protected HashMap<String, Object> HumanAgentData = new HashMap<String, Object>();
	protected HashMap<String, Object> RobotAgentData = new HashMap<String, Object>();
	protected HashMap<String, Object> WarehouseAgentData = new HashMap<String, Object>();

	       
	public AgentFactoryConfig(String config) throws IOException {
		
    	BufferedReader br;
    	
		br = new BufferedReader( new FileReader("AgentFactory.config"));
			
		String line;
		while((line = br.readLine()) != null) {
			System.out.println(line);
		}
		br.close();
		
	}
	
}
