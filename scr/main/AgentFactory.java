package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import objects.Article;

public class AgentFactory {
	
	private static final int HumanAgent = 2;
	private static final int RobotAgent = 2;
	
    public static void main(String[] args) throws IOException {

    	// AgentFactoryConfig data = new AgentFactoryConfig("AgentFactory.config");
    	
    	// configure agent plattform
    	PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
	    	config.setPlatformName("Picking System");
	    	config.getRootConfig().setNetworkName("MyPickingSystemNetworkName");
	    	config.getRootConfig().setNetworkPass("MyPickingSystemNetworkPassword");
	    	config.getRootConfig().setTrustedLan(true);
	    	config.getRootConfig().setAwareness(true);
	    	//config.getRootConfig().setAwaMechanisms(PlatformConfiguration.AWAMECHANISM.local, PlatformConfiguration.AWAMECHANISM.broadcast, PlatformConfiguration.AWAMECHANISM.multicast);
	    
	    // start agent plattform
    	IExternalAccess platform = Starter.createPlatform(config).get();   	
        IComponentManagementService cms = SServiceProvider.getService(platform, IComponentManagementService.class).get();     
        System.out.println("");
        
        // create data server
        HashMap<String, Object> DataServerAgentData = new HashMap<String, Object>();
        DataServerAgentData.put("argServer", "127.0.0.1");
        DataServerAgentData.put("argDatabase", "pickingstation");
        DataServerAgentData.put("argUser", "admin");
        DataServerAgentData.put("argPassword", "admin");
        cms.createComponent("Data Server", "agents.DataServerAgentBDI.class", new CreationInfo(DataServerAgentData)).getFirstResult();
 
        // create articles       
        List<Article> Articles = new ArrayList<Article>();
        Articles.add(new Article(1 , "Sto-Kompliment", 					"", 		"A1",  23.5, "", "C:\\Photoneo\\D-Data\\01"));
        Articles.add(new Article(2 , "HSU Technikfreak Tasse", 			"Tasse", 	"B2",  19.3, "", "C:\\Photoneo\\D-Data\\02"));
        Articles.add(new Article(3 , "HSU inGENIEur Tasse", 			"Tasse", 	"C2",   0.0, "", "C:\\Photoneo\\D-Data\\03"));
        Articles.add(new Article(4 , "HSU Fakultaet P Tasse", 			"Tasse", 	"B4",  90.8, "", "C:\\Photoneo\\D-Data\\04"));
        Articles.add(new Article(5 , "HSU WIP Tasse", 					"Tasse", 	"C3",  87.2, "", "C:\\Photoneo\\D-Data\\05"));
        Articles.add(new Article(6 , "Nutella Glas 75 g gratis", 		"", 		"D2",  54.4, "", "C:\\Photoneo\\D-Data\\06"));
        Articles.add(new Article(7 , "Latte Macchiato Tizio", 			"Becher", 	"E2",  65.8, "", "C:\\Photoneo\\D-Data\\07"));
        Articles.add(new Article(8 , "Latte Macchiato Gut & Guenstig", 	"Becher", 	"D3",  30.6, "", "C:\\Photoneo\\D-Data\\08"));
        Articles.add(new Article(9 , "Schaltschrankschluessel", 		"", 		"E4",   0.0, "", "C:\\Photoneo\\D-Data\\09"));
        Articles.add(new Article(10, "Ladegeraet universell IWL", 		"", 		"F1",  73.3, "", "C:\\Photoneo\\D-Data\\10"));
        Articles.add(new Article(11, "Cola Mix Aloisius Quelle leer",	"Flasche", 	"F3",  76.9, "", "C:\\Photoneo\\D-Data\\11"));
        Articles.add(new Article(12, "Tannenzaepfle Rothaus", 			"Flasche", 	"F5",  92.8, "", "C:\\Photoneo\\D-Data\\12"));
        /** Die Artikel k�nnenauch direkt aus der Datenbank geladen werden - das m�sste aber in jedem Agenten einzeln implementiert werden, siehe ComputationClusterAgent. **/
        
        // create monitor system
        HashMap<String, Object> MonitorAgentData = new HashMap<String, Object>();
        MonitorAgentData.put("argPathSaveRecord", "C:\\Photoneo\\Simulation\\Protokoll");
        cms.createComponent("MonitorSystem", "agents.MonitorAgentBDI.class", new CreationInfo(MonitorAgentData)).getFirstResult();
       
        /*
        // create computation cluster
        HashMap<String, Object> ComputationClusterAgentData = new HashMap<String, Object>();
        ComputationClusterAgentData.put("argPathDarknet", "C:\\darknet\\Debug");
        cms.createComponent("Computation Cluster", "agents.ComputationClusterAgentBDI.class", new CreationInfo(ComputationClusterAgentData)).getFirstResult();    
        
		System.out.println("");

		*/

        // create human picker
        HashMap<String, Object> HumanAgentData = new HashMap<String, Object>();
        HumanAgentData.put("argArticles", Articles);
        HumanAgentData.put("argWayPoint", "TransferStation");
        HumanAgentData.put("argMovementSpeed", 1.7);
        HumanAgentData.put("argTimePickArticle", 10);
        HumanAgentData.put("argTimeDeliverPickingOrder", 10);
        HumanAgentData.put("argCapacity", 2);
        HumanAgentData.put("argSpeedFactor", 1);
        HumanAgentData.put("argType", "Human");
        HumanAgentData.put("argMaxSizeOrderList", 10);
        for (int i=1; i<=HumanAgent; i++)
        	cms.createComponent("HumanPicker-" + String.valueOf(i), "agents.HumanAgentBDI.class", new CreationInfo(HumanAgentData)).getFirstResult();

        // create robot picker
        HashMap<String, Object> RobotAgentData = new HashMap<String, Object>();
        RobotAgentData.put("argArticles", Articles);
        RobotAgentData.put("argWayPoint", "TransferStation");
        RobotAgentData.put("argMovementSpeed", 0.85);
        RobotAgentData.put("argTimePickArticle", 20);
        RobotAgentData.put("argTimeDeliverPickingOrder", 30);
        RobotAgentData.put("argCapacity", 2);
        RobotAgentData.put("argDelayEmergencyCall", 129);
        RobotAgentData.put("argPathDarknet", "C:\\darknet\\Debug");
        RobotAgentData.put("argPathData", "data\\obj.data");
        RobotAgentData.put("argPathConfig", "data\\yolo-obj.cfg");
        RobotAgentData.put("argPathWeights", "C:\\Photoneo\\Simulation\\Modelle_Objekterkennung\\yolo-obj_8000.weights");
        RobotAgentData.put("argThresholdObjectDetection", 95.0);
        RobotAgentData.put("argPathYoloMark", "C:\\YoloMark\\x64\\Release");
        RobotAgentData.put("argSpeedFactor", 1);       
        RobotAgentData.put("argTimeEvaluateSituation", 10);
        RobotAgentData.put("argTimeModifyEnvironment", 10);
        RobotAgentData.put("argTimeConfirmNewRecord", 5);
        RobotAgentData.put("argTimeConfirmRelease", 5);
        RobotAgentData.put("argTimeMarkArticle", 12);
        RobotAgentData.put("argType", "Robot");
        RobotAgentData.put("argMaxSizeOrderList", 10);
        for (int i=1; i<=RobotAgent; i++)
        	cms.createComponent("RobotPicker-" + String.valueOf(i), "agents.RobotAgentBDI.class", new CreationInfo(RobotAgentData)).getFirstResult();
		
		System.out.println("");
        
		// create warehouse management system
        HashMap<String, Object> WarehouseAgentData = new HashMap<String, Object>();
        WarehouseAgentData.put("argArticles", Articles);
        WarehouseAgentData.put("argMaxPosition", 5);
        WarehouseAgentData.put("argMaxQuantity", 1);
        WarehouseAgentData.put("argInterarrivalTime", 5);
        WarehouseAgentData.put("argSpeedFactor", 1);
        WarehouseAgentData.put("argWorkingTime", 3600); // <-- Wert aus HICL2020-Paper [s]
        WarehouseAgentData.put("argPathOrderList", "C:\\Photoneo\\Simulation\\PickingOrders");
        WarehouseAgentData.put("argPreselectionOrderAssignment", false);
        WarehouseAgentData.put("argIntervalRollingHorizons", 60); //[s]
        WarehouseAgentData.put("argL_EC_H", 61); // <-- Wert aus HICL2020-Paper [s]
        WarehouseAgentData.put("argL_EC_R", 94); // <-- Wert aus HICL2020-Paper [s]
        WarehouseAgentData.put("argC_H", HumanAgent * 0.07667); // <-- Wert aus HICL2020-Paper [picks/s]
        WarehouseAgentData.put("argC_R", RobotAgent * 0.025); // <-- Wert aus HICL2020-Paper [picks/s]
        cms.createComponent("WMS", "agents.WarehouseAgentBDI.class", new CreationInfo(WarehouseAgentData)).getFirstResult();
        
    }
    
}